# step-by-step

## node

v18.16.0

## yarn

yarn 源地址 <https://registry.npm.taobao.org>

## Project Setup

```sh
yarn install
```

### Compile and Hot-Reload for Development

```sh
yarn run dev
```

### Type-Check, Compile and Minify for Production

```sh
yarn run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
yarn run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
yarn run lint
```
