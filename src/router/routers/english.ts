const sqlRouters = [
    {
        path: '/english/0001',
        name: '单数代词及a/an的用法',
        component: () => import('@/views/english/0001/Index.vue'),
        show:true,
    },
    {
        path: '/english/0002',
        name: '复数代词及复数名词的用法',
        component: () => import('@/views/english/0002/Index.vue'),
        show:true,
    },
    {
        path: '/english/0003',
        name: '形容词的用法',
        component: () => import('@/views/english/0003/Index.vue'),
        show:true,
    }
]
export default sqlRouters;