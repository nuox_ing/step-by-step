const npmRouters = [
    {
        path: '/npm/0001',
        name: 'react-create-app',
        component: () => import('@/views/npm/0001/Index.vue'),
        show:true,
    },
    {
        path: '/npm/0002',
        name: 'immutable',
        component: () => import('@/views/npm/0002/Index.vue'),
        show:true,
    },
    {
        path: '/npm/0003',
        name: 'immer',
        component: () => import('@/views/npm/0003/Index.vue'),
        show:true,
    }
]
export default npmRouters;