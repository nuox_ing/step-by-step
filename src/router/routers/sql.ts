const sqlRouters = [
    {
        path: '/sql/0001',
        name: '数据库表的相关操作',
        component: () => import('@/views/sql/0001/Index.vue'),
        show:true,
    },
    {
        path: '/sql/0002',
        name: '数据类型',
        component: () => import('@/views/sql/0002/Index.vue'),
        show:true,
    }
]
export default sqlRouters;