const jRouters = [
    {
        path: '/javascript/0001',
        name: '数据类型及检测方式',
        component: () => import('@/views/javascript/0001/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0002',
        name: 'This',
        component: () => import('@/views/javascript/0002/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0003',
        name: 'bind、call、apply',
        component: () => import('@/views/javascript/0003/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0004',
        name: '作用域',
        component: () => import('@/views/javascript/0004/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0005',
        name: '闭包',
        component: () => import('@/views/javascript/0005/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0006',
        name: '函数科里化',
        component: () => import('@/views/javascript/0006/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0007',
        name: '面向对象',
        component: () => import('@/views/javascript/0007/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0008',
        name: '继承',
        component: () => import('@/views/javascript/0008/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0009',
        name: '原型原型链',
        component: () => import('@/views/javascript/0009/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0010',
        name: 'New',
        component: () => import('@/views/javascript/0010/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0011',
        name: 'Set',
        component: () => import('@/views/javascript/0011/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0012',
        name: '提升',
        component: () => import('@/views/javascript/0012/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0013',
        name: 'Object',
        component: () => import('@/views/javascript/0013/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0014',
        name: '树和数组互转',
        component: () => import('@/views/javascript/0014/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0015',
        name: 'Number',
        component: () => import('@/views/javascript/0015/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0016',
        name: 'null和undefined',
        component: () => import('@/views/javascript/0016/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/0017',
        name: '类型转换',
        component: () => import('@/views/javascript/0017/Index.vue'),
        show:true,
    },
    {
        path: '/javascript/2001',
        name: '1loc Object',
        component: () => import('@/views/javascript/2001/Index.vue'),
        show:true,
    }
]
export default jRouters;