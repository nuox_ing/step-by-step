const vRouters = [
    {
        path: '/vue/0001',
        name: '插槽 Slots',
        component: () => import('@/views/vue/0001/Index.vue'),
        show:true,
    },
    {
        path: '/vue/0001/options',
        name: '插槽 Slots options',
        component: () => import('@/views/vue/0001/options/Index.vue'),
        show:false
    },
    {
        path: '/vue/0001/composition',
        name: '插槽 Slots composition',
        component: () => import('@/views/vue/0001/composition/Index.vue'),
        show:false
    },
    {
        path: '/vue/0002',
        name: 'mixins',
        component: () => import('@/views/vue/0002/Index.vue'),
        show:true,
    },
]
export default vRouters;