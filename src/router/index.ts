import { createRouter, createWebHistory } from 'vue-router'
import englishRouters from './routers/english'
import HomeView from '../views/HomeView.vue'
import vRouters  from './routers/vue'
import jRouters from './routers/javascript'
import sqlRouters from './routers/sql'
import reactRouters from './routers/react'
import npmRouters from './routers/npm'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: '首页',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('../views/AboutView.vue')
    },
    ...vRouters,
    ...jRouters,
    ...sqlRouters,
    ...reactRouters,
    ...npmRouters,
    ...englishRouters
  ]
})
router.beforeEach((to, from, next) => {
  if (to?.name) {
    document.title = to.name as string
  }
  next()
})

export default router
