# 0011面向对象

封装、多态、继承

## 面向对象编程思想

### 多态

JavaScript 所谓的“鸭子类型”是指一种动态类型语言的编程风格，通常用于形容在**运行时**，只要一个对象具有某些方法或属性，就可以认为它是某种类型的实例，即不要求严格的类型一致性，只要形状相似就可以认为是同一类型。
JavaScript 中，由于语言的动态性，对象的类型是在运行时决定的，而不是在编译时静态确定的。因此，我们可以用同样的方式对待不同形状的对象。（Taro 内部实现也有编译时+运行时）
我们无需强制限制一个对象必须是某个特定类的实例，而是可以根据其形状和行为来组织代码。这在很多场合下非常有用，比如在写函数时接受任意类型的参数，或者实现面向对象的继承时允许多态性。

```js
function p() {
    console.log('p')
}
function hasPrint(obj) {//判断对象里有没有p方法
    return obj && typeof obj.p === 'function'
}
let obj1 = { a: 123, p }
let obj2 = { b: '你还好么' }
let obj3 = { c: 567, p }
console.log(hasPrint(obj1))
console.log(hasPrint(obj2))
console.log(hasPrint(obj3))
//设计的本质就是向上抽象（对比java）:类 →抽象类→ 接口
// 比入ts的interface
// interface Person{
//     run(a:number):string;
//     eat():void;
// }
```

### 封装

封装是指将对象的状态（即属性）和行为（即方法）包装在一起，对外隐藏对象的部分属性和方法，只保留对外公开的接口。这样可以保护对象内部的状态不受外界干扰，从而更好地控制对象的访问和修改。
在以前，我们通常会使用闭包等形式去实现这样一个封装的处理
在 JavaScript 中，常见的封装方式是使用闭包来封装对象的私有属性和方法。  

```js
function Add() {
    this.num = 1
    this.add=function(){
        this.num++
    }
    this.get=function(){
        return this.num
    }
}
let a=new Add()
a.add()
console.log(a.get())
```

### 继承

详细内容查看：0008继承
