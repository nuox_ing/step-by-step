# Number

## question

- 0.1 + 0.2 = ?

## 0.1+0.2=?

```js
var a = 0.1 + 0.2
console.log(a)//0.30000000000000004
console.log(a === 0.3)//false
```

二进制浮点数中0.1和0.2都是无限循环的数字，所以在计算机中无法精确表示，只能近似表示，所以0.1+0.2不等于0.3

### 怎么判断 0.1 + 0.2 = 0.3 呢？

最常见的方法是设置一个误差范围值，通常被称为“机器精度”，对于JavaScript，这个值通常是2^-52（2.220446049250313e-16）。
ES6提供了Number.EPSILON常量，表示1与大于1的最小浮点数之间的差。

```js
function numbersCloseEnoughToEqual(n1,n2){
    return Math.abs(n1-n2)<Number.EPSILON
}
var a=0.1+0.2;
var b=0.3
console.log(numbersCloseEnoughToEqual(a,b))//true
```
