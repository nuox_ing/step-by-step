# 原型原型链

## question

- 说下原型原型链
- 怎么理解prototype、__proto__、constructor，他们之间的关系是什么

## __proto__和prototype关系

__proto__和constructor是对象独有的。prototype属性是函数独有的
在 js 中我们是使用构造函数来新建一个对象的，每一个构造函数的内部都有一个 prototype 属性值，这个属性值是一个对象，这个对象包含了可以由该构造函数的所有实例共享的属性和方法。当我们使用构造函数新建一个对象后，在这个对象的内部将包含一个指针，这个指针指向构造函数的 prototype 属性对应的值，在 ES5 中这个指针被称为对象的原型。一般来说我们是不应该能够获取到这个值的，但是现在浏览器中都实现了 proto 属性来让我们访问这个属性，但是我们最好不要使用这个属性，因为它不是规范中规定的。ES5 中新增了一个 Object.getPrototypeOf() 方法，我们可以通过这个方法来获取对象的原型。

## 原型(prototype)

一个简单的对象，用于实现对象的 属性继承。可以简单的理解成对象的爹。在 Firefox 和 Chrome 中，每个JavaScript对象中都包含一个__proto__(非标准)的属性指向它爹(该对象的原型)，可obj.__proto__进行访问。

## 原型链

原型链是由原型对象组成，每个对象都有 __proto__ 属性，指向了创建该对象的构造函数的原型，__proto__ 将对象连接起来组成了原型链。是一个用来实现继承和共享属性的有限的对象链
属性查找机制: 当查找对象的属性时，如果实例对象自身不存在该属性，则沿着原型链往上一级查找，找到时则输出，不存在时，则继续沿着原型链往上一级查找，直至最顶级的原型对象Object.prototype，如还是没找到，则输出undefined；
属性修改机制: 只会修改实例对象本身的属性，如果不存在，则进行添加该属性，如果需要修改原型的属性时，则可以用: b.prototype.x = 2；但是这样会造成所有继承于该对象的实例的属性发生改变。

## 构造函数(constructor)

可以通过new来 新建一个对象 的函数。

## 实例

通过构造函数和new创建出来的对象，便是实例。 实例通过__proto__指向原型，通过constructor指向构造函数。

实例.__proto__ === 原型
原型.constructor === 构造函数
构造函数.prototype === 原型

```js
var a={};
a.__proto__===Object.prototype;//true
a.constructor===Object;//true

```

### js 获取原型的方法

  p.proto  
  p.constructor.prototype  
  Object.getPrototypeOf(p)  

### 总结

每个函数都有 prototype 属性，除了 Function.prototype.bind()，该属性指向原型。  
每个对象都有 __proto__ 属性，指向了创建该对象的构造函数的原型。其实这个属性指向了 [[prototype]]，但是 [[prototype]]是内部属性，我们并不能访问到，所以使用 _proto_来访问。  
对象可以通过 __proto__ 来寻找不属于该对象的属性，__proto__ 将对象连接起来组成了原型链。  
原型链的尽头一般都是 null，但是我们不能说原型链的尽头就是 null，因为这样说是不严谨的，原型链的尽头是没有任何东西的，是一个空指针，当我们访问一个对象不存在的属性时，那么就会沿着原型链一直查找下去，直到原型链的尽头，如果在原型链的尽头还是没有找到，则会返回 undefined。
