# 树和数组互转

## question

- 怎么把树转为数组
- 怎么把数组转为树

## 数组数据

```js
var array = [
    { id: 1, pId: 0 }, { id: 2, pId: 1 }, { id: 3, pId: 0 },
    { id: 4, pId: 3 }, { id: 5, pId: 3 }, { id: 6, pId: 1 },
    { id: 7, pId: 4 }, { id: 8, pId: 7 }, { id: 9, pId: 8 },
]
```

## 数组转树

### V1 利用递归

每次遍历时，找到将本次遍历的根节点作为父节点的所有子节点，直至找不到有子节点的，此时，filter 返回空数组，递归停止。

```js
var arrayToTreeV1 = (items, root) => items.filter(i => i.pId === root)
.map(item => ({ ...item, children: arrayToTree(array, item.id) }))
```

### V2 利用 浅拷贝

浅拷贝是拷贝对象的内存地址，只要修改，所有引用都会同步修改。利用这个特点，我们将子节点依次放入父节点，最后将最外层父节点返回即可

```js
const arrayToTreeV2 = (list, root) => {
    const result = []
    const map = {}
    for (const item of list) {
        map[item.id] = { ...item }
    }
    for (const item of list) {
        const { id, pId } = item
        if (item.pId === root) {
            result.push(map[id])
        } else {
            map[pId].children
                ? map[pId].children.push(map[id])
                : (map[pId].children = [map[id]])
        }
    }
    return result
}
```

## 树转数组

### V1 使用递归

```js
const treeToArrayV1 = (tree, result = []) => {
    tree.forEach(({ children, ...item }) => {
        result.push(item)
        children && treeToArrayV1(children, result)
    })
    return result
}
```

### V2 利用栈

```js
const treeToArrayV2 = tree => {
    const result = []
    const stack = [...tree]
    while (stack.length) {
        const item = stack.pop()
        const { children, ...data } = item
        result.push(data)
        children && stack.push(...children)
    }
    return result
}
```
