# 提升

## question

- 什么是提升
- 函数和变量提升优先级

## 什么是提升

声明从他们代码中出现的位置，被移动到最上面，这个过程叫做提升。

## 先有蛋（声明）、后有鸡（赋值）

只有声明本身会被提升，而赋值或其他运行逻辑会留在原地。

```js
a = 2;
var a;
console.log(a); // 2
//因为变量提升，所以这个代码实际上是这样处理的：
var a;
a = 2;
console.log(a); // 2
```

## 函数声明会被提升，但是函数表达式不会被提升

```js
foo(); // 1
var foo;
function foo() {
  console.log(1);
}
foo = function () {
  console.log(2);
};
```

## 具名的函数表达式，名称标识符在赋值之前是不能被引用的

```js
foo(); // TypeError
bar(); // ReferenceError
var foo = function bar() {
  // ...
};
```

## 函数优先

函数声明和变量声明都会被提升。但是一个值得注意的细节（这个细节可以出现在有多个“重复”声明的代码中）是函数会首先被提升，然后才是变量。

```js
foo(); // 3
function foo() {
  console.log(1);
}
var foo = function () {
  console.log(2);
};
function foo() {
  console.log(3);
}
```

## 面试题

```js
var foo=function(){
    console.log(1)
}
foo()
var foo=function(){
    console.log(2)
}
foo()
function foo(){
    console.log(3)
}
foo()
function foo(){
    console.log(4)
}
foo()
//1 2 2 2
```
