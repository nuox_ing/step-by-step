# 类型转换

## question

- 什么是显式转换（类型转换），什么是隐式转换（强制类型转换）

## 抽象值操作

### ToString

null转换为"null"，undefined转换为"undefined"，true转换为"true"，false转换为"false"，数字转换为相应的字符串,遵循以下规则：极小和极大的数字使用指数形式，对象（包括数组）会首先被转换为相应的基本类型值，如果没有toString()方法或者toString()方法返回的不是基本类型值，则会调用valueOf()方法，如果valueOf()方法返回的还不是基本类型值，则会抛出错误

#### JSON.stringify()

JSON.stringify()并非严格意义上的强制类型转换
所有安全的JSON值都可以JSON.stringify()转换为对应的字符串，不安全（undefined、function、symbol（ES6+））都会被忽略，如果对象中存在非安全的JSON值，会忽略，如果数组中存在非安全的JSON值，则会返回null(保证单元位置不变)，如果对象中定义了toJSON()方法（返回一个能够被字符串化的安全JSON值，而不是返回一个JSON字符串），则会先调用该方法，然后再进行序列化，如果toJSON()方法返回undefined，则会忽略该值，如果返回的是函数，则会报错,如果包含循环引用，则会报错

```js
var o = {}
var a = {
    b: 42,
    c: o,
    d: function () { }
}
o.e = a;
// JSON.stringify(a)//报错
a.toJSON=function(){
    return {b:this.b}
}
console.log(JSON.stringify(a))//'{"b":42}'
```

## 隐式转换

## 显式转换（强制类型转换）

js中强制类型转换总是返回基本类型值，不会返回引用类型值

### 显式解析数字字符串

## 总结

### 区别

类型转换发生在静态类型的编译阶段，而强制类型转换发生在动态类型的运行阶段
