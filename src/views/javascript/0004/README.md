# 作用域 作用域链

变量的作用域分为两种：词法作用域(静态作用域)和动态作用域。词法作用域指的是变量只能在定义它的代码块内使用，一旦离开该代码块，变量就会消失。而动态作用域则是指变量可以在函数内部被重新定义，并且可以在不同的函数调用之间共享，javascript是词法作用域
在 JavaScript 中，函数内部的变量被称为局部变量，而函数外部的变量被称为全局变量。局部变量只能在函数内部被访问和修改，而全局变量则可以在任何地方被访问和修改。
为了解决作用域问题，JavaScript 还提供了一些特殊的语法结构，如evel、 with 语句、IIFE(立即执行函数表达式、即调函数)等。with 语句可以用来简化对全局变量的引用；IIFE 则可以将函数的作用域限制在当前代码块内部，避免污染全局命名空间。

## 作用域

作用域是定义变量的区域，它有一套访问变量的规则，这套规则来管理浏览器引擎如何在当前作用域以及嵌套的作用域中根据变量（标识符）进行变量查找

### 全局作用域

全局变量是挂载在 window 对象下的变量，所以在网页中的任何位置你都可以使用并且访问到这个全局变量

```js
var globalName = 'global';
function getName() { 
  console.log(globalName) // global
  var name = 'inner'
  console.log(name) // inner
} 
getName();
console.log(name); // 
console.log(globalName); //global
function setName(){ 
  vName = 'setName';
}
setName();
console.log(vName); // setName
```

无论在什么地方都是可以被访问到的，所以它就是全局变量
当然全局作用域有相应的缺点，我们定义很多全局变量的时候，会容易引起变量命名的冲突，所以在定义变量的时候应该注意作用域的问题

### 函数作用域

函数中定义的变量叫作函数变量，这个时候只能在函数内部才能访问到它，所以它的作用域也就是函数的内部，称为函数作用域
除了这个函数内部，其他地方都是不能访问到它的。同时，当这个函数被执行完之后，这个局部变量也相应会被销毁。

```js
function getName () {
  var name = 'inner';
  console.log(name); //inner
}
getName();
console.log(name);//undefined
```

### 块级作用域

ES6 中新增了块级作用域，最直接的表现就是新增的 let 关键词，使用 let 关键词定义的变量只能在块级作用域中被访问，有“暂时性死区”的特点，也就是说这个变量在定义之前是不能被使用的。
在 JS 编码过程中 if 语句及 for 语句后面以及 {...} 这里面所包括的，就是块级作用域

```js
console.log(a) //a is not defined
if(true){
  let a = '123'；
  console.log(a)； // 123
}
console.log(a) //a is not defined

{
    var a=10
}
console.log(a)  //10

{
    let a=10
}
console.log(a)  //a is not defined
```

#### 块及作用域的替代方案

```js
{
  let a = 2;
  console.log(a); //2
}
console.log(a); //ReferenceError: a is not defined
//es6之前怎么实现块级作用域
try{
  throw 2;
}catch(a){
  console.log(a); //2
}
console.log(a); //ReferenceError: a is not defined
```

#### 块及作用域的其他形式

1. try/catch 的 catch 分句
2. with 语句

### 暂时性死区

在代码块内，使用let命令声明变量之前，该变量都是不可用的。这在语法上，称为“暂时性死区”（temporaldeadzone，简称TDZ

## IIFE(Immediately Invoked Functions Expressions)

定义时就会立即执行的 JavaScript 函数

```js
//第一种写法(function foo(){})()
var a = 2;
(function foo() {
    var a = 3;
    console.log(a);//3
})();
console.log(a)//2
//第二种写法(function foo(){}())
var a = 2;
(function foo() {
    var a = 3;
    console.log(a);//3
}());
console.log(a)//2
//传递参数
var a = 2;
(function IIFE(global){
    var a=3;
    console.log(a);//3
    console.log(global.a);//2
})(window);
//倒置代码顺序，将需要运行的函数放在第二位，在IIFE之前先执行执行后当作参数传递进去，在UMD模式中经常使用
(function IIFE(def){
    def(window)
})(function def(global){
    var a=3;
    console.log(a);
    console.log(global.a);
})
```

## 作用域链

作用域链的作用是保证对执行环境有权访问的所有变量和函数的有序访问，通过作用域链，我们可以访问到外层环境的变量和 函数。

作用域链的本质上是一个指向变量对象的指针列表。变量对象是一个包含了执行环境中所有变量和函数的对象。作用域链的前 端始终都是当前执行上下文的变量对象。全局执行上下文的变量对象（也就是全局对象）始终是作用域链的最后一个对象。[[Scopes]]

当我们查找一个变量时，如果当前执行环境中没有找到，我们可以沿着作用域链向后查找
作用域链的创建过程跟执行上下文的建立有关....

## with 语句

with 语句的作用是将代码的作用域设置到一个特定的对象中。with 语句的语法如下：
不推荐使用，在严格模式禁止使用

```js
with (expression) statement
```

## eval 函数

eval 函数的作用是将传入的字符串当作 JavaScript 代码进行执行。
不推荐使用，在严格模式禁止使用

```js
var evl="var a = 2";
(function fn(){
    eval(evl)
    console.log(a)//2
})()
```

## 拓展（词法作用域和动态作用域）

```js
function foo(){
    console.log(a)//2
}
function bar(){
    var a=3;
    foo()
}
var a=2;
bar()
//因为javascrpt采用的是词法作用域，所以输出是2，如果是动态作用域，那么输出就是3
```
