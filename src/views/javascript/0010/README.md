# New

## question

- new创建对象做了什么
- 怎么实现一个new函数

## new创建对象做了什么

1. 创建一个新对象。
2. 将新对象的原型设置为构造函数的 prototype 属性。
3. 将新对象作为 this 对象调用构造函数。
4. 如果构造函数返回一个对象，则返回该对象；否则返回这个新对象。

## 手写new
  
```js
function myNew(constructor, ...args) {
    let obj = Object.create(constructor.prototype)
    let result = constructor.apply(obj, args)
    return result instanceof Object ? result : obj
}

function Person(name, age) {
    this.name = name
    this.age = age
}
Person.prototype.sayName = function () {
    console.log(this.name, this.age)
}
const a = new Person('小芳', 18)
console.log(a)//Person {name: '小芳', age: 18}
console.log(a.sayName())//小芳 18

const b=myNew(Person,'小明',20)
console.log(b)//Person {name: '小明', age: 20}
console.log(b.sayName())//小明 20
```
