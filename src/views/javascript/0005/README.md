# 闭包

## question

- 说下什么是闭包
- 闭包解决了什么问题
- 闭包会造成什么问题
- 怎么避免闭包造成的内存泄漏
- 如何解决var+setTimeout循环输出问题？

## 什么是闭包

当前函数可以记住并访问所在的词法作用域时，就产生了闭包，即使函数是在当前词法作用域之外执行。
本质就是：当前环境中存在指向父级作用域的引用
闭包其实就是一个可以访问其他函数内部变量的函数。创建闭包的最常见的方式就是在一个函数内创建另一个函数，创建的函数可以 访问到当前函数的局部变量。

## 期望使用闭包解决什么问题

1. 闭包可以访问函数内部的变量（私有变量）
2. 闭包可以让这些变量的值始终保存在内存中、缓存计算（react中常用）
3. 闭包可以实现封装，防止变量被全局污染

### 题目一

```js
  function fun1() {
    var a = 1;
    return function(){
      console.log(a);
      return a
    };
  }
  fun1();
  var result = fun1();
  console.log(result());  // 1
```

// 结合闭包的概念，我们把这段代码放到控制台执行一下，就可以发现最后输出的结果是 1（即 a 变量的值）。那么可以很清楚地发现，a 变量作为一个 fun1 函数的内部变量，正常情况下作为函数内的局部变量，是无法被外部访问到的。但是通过闭包，我们最后还是可以拿到 a 变量的值

### 题目二

```js
let a = 1
var b = 2
// fn 是闭包
function fn() {
  console.log(a, b);
}
```

![avatar](/javascript/0005/1.jpeg)

从图中我们能发现全局下声明的变量，如果是 var 的话就直接被挂到 globe 上，如果是其他关键字声明的话就被挂到 Script 上。虽然这些内容同样还是存在 [[Scopes]]，但是全局变量应该是存放在静态区域的，因为全局变量无需进行垃圾回收，等需要回收的时候整个应用都没了。

## 闭包产生的原因

当访问一个变量时，代码解释器会首先在当前的作用域查找，如果没找到，就去父级作用域去查找，直到找到该变量或者不存在父级作用域中，这样的链路就是作用域链
需要注意的是，每一个子函数都会拷贝上级的作用域，形成一个作用域的链条

## 闭包的使用场景

1、返回一个函数
2、在定时器、事件监听、Ajax 请求、Web Workers 或者任何异步中，只要使用了回调函数，实际上就是在使用闭包。如：
2、函数科里化、单列模式、工厂模型

```js
// 定时器
setTimeout(function handler(){
  console.log('1');
}，1000);

// 事件监听
('#app').click(function(){
  console.log('Event Listener');
});

//作为函数参数传递的形式
var a = 1;
function bar(fn){
  fn();
}
function foo(){
  var a = 2;
  function baz(){
    console.log(a);
  }
  bar(baz);
}
foo();  // 输出2，而不是1

//IIFE（立即执行函数），创建了闭包，保存了全局作用域（window）和当前函数的作用域，因此可以输出全局的变量
var a = 2;
(function IIFE(){
  console.log(a);  // 输出2
})();
//IIFE 这个函数会稍微有些特殊，算是一种自执行匿名函数，这个匿名函数拥有独立的作用域。这不仅可以避免了外界访问此 IIFE 中的变量，而且又不会污染全局作用域，我们经常能在高级的 JavaScript 编程中看见此类函数。
```

## 如何解决循环输出问题？

```js
for(var i = 1; i <= 5; i ++){
  setTimeout(function() {
    console.log(i)
  }, 0)
}
```

setTimeout 为宏任务，由于 JS 中单线程 eventLoop 机制，在主线程同步任务执行完后才去执行宏任务，因此循环结束后 setTimeout 中的回调才依次执行
因为 setTimeout 函数也是一种闭包，往上找它的父级作用域链就是 window，变量 i 为 window 上的全局变量，开始执行 setTimeout 之前变量 i 已经就是 6 了，因此最后输出的连续就都是 6。

如何按顺序依次输出 1、2、3、4、5 呢？

//可以利用 IIFE（立即执行函数），当每次 for 循环时，把此时的变量 i 传递到定时器中，然后执行，改造之后的代码如下。

```js
for(var i = 1;i <= 5;i++){
  (function(j){
    setTimeout(function timer(){
      console.log(j)
    }, 0)
  })(i)
}
```

//ES6 中新增的 let 定义变量的方式，使得 ES6 之后 JS 发生革命性的变化，让 JS 有了块级作用域，代码的作用域以块级为单位进行执行。通过改造后的代码，可以实现上面想要的结果。

```js
for(let i = 1; i <= 5; i++){
  setTimeout(function() {
    console.log(i);
  },0)
}
```

//定时器传入第三个参数

```js
for(var i=1;i<=5;i++){
  setTimeout(function(j) {
    console.log(j)
  }, 0, i)
}
```
