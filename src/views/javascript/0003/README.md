# apply、call、bind

都是为了改变this的指向

## call

fn.call(thisArg, param1, param2, ...)
使用一个指定的 this 值和单独给出的一个或多个参数来调用一个函数。

### 场景1（call）

```js
//通过对象原型上toString方法来判断数据类型
Object.prototype.toString.call(123) // "[object Number]"
```

### 场景2（call）

```js
//类数组转换为数组
Array.prototype.slice.call({0:'gg',1:'love',2:'meimei',length:3}) // ["gg", "love", "meimei"]
Array.form() //使用ES6新增方法
```

### 场景3（call）

```js
//构造函数实现继承
function Product(name, price) {
  this.name = name;
  this.price = price;
}
function Food(name, price) {
  Product.call(this, name, price);
  this.category = 'food';
}
console.log(new Food('cheese', 5).name);  // "cheese"
```

### 实现call

```js
Function.prototype.myCall = function (context) {
  var context = context || window
  // 给 context 添加一个属性
  // getValue.call(a, 'pp', '24') => a.fn = getValue
  context.fn = this
  // 将 context 后面的参数取出来
  var args = [...arguments].slice(1)
  // getValue.call(a, 'pp', '24') => a.fn('pp', '24')
  var result = context.fn(...args)
  // 删除 fn
  delete context.fn
  return result
}
```

## apply

fn.apply(thisArg, [param1,param2,...])
使用一个指定的 this 值和一个数组（或者类数组对象）来调用一个函数。

### 场景1（apply）

```js
//获取数组的最大 / 最小值
let arr = [13, 6, 10, 11, 16];
const max = Math.max.apply(Math, arr); //16
const min = Math.min.apply(Math, arr);  //6
```

### 实现apply

```js
Function.prototype.myApply = function(context = window, ...args) {
  // this-->func  context--> obj  args--> 传递过来的参数

  // 在context上加一个唯一值不影响context上的属性
  let key = Symbol('key')
  context[key] = this; // context为调用的上下文,this此处为函数，将这个函数作为context的方法
  // let args = [...arguments].slice(1)   //第一个参数为obj所以删除,伪数组转为数组
  
  let result = context[key](...args[0]); 
  delete context[key]; // 不删除会导致context属性越来越多
  return result;
}
```

## bind

fn.bind(thisArg, param1, param2, ...) //返回待执行函数

### 实现bind

```js
Function.prototype.myBind = function (context) {
  if (typeof this !== 'function') {
    throw new TypeError('Error')
  }
  var _this = this
  var args = [...arguments].slice(1)
  // 返回一个函数
  return function F() {
    // 因为返回了一个函数，我们可以 new F()，所以需要判断
    if (this instanceof F) {
      return new _this(...args, ...arguments)
    }
    return _this.apply(context, args.concat(...arguments))
  }
}
```

## 总结

### 相同点

1. 都是用来改变函数的this对象的指向的。
2. 第一个参数都是this要指向的对象。
3. 都可以利用后续参数传参。

### 不同点

1. call()与 apply() 方法类似，只有一个区别，就是 call() 方法接受的是一个参数列表，而 apply() 方法接受的是一个包含多个参数的数组。
2. bind()与call()接受参数相同，区别是bind()会返回一个函数。并且我们可以通过bind实现柯里化。
