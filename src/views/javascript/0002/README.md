# This

1. this永远指向一个对象
2. this指向跟调用有关，跟定义无关
3. 通过call、apply、bind修改this指向
4. 箭头函数中的 this 只取决包裹箭头函数的第一个普通函数的 this。

由于 JS 的设计原理：
**在函数中，可以引用运行环境中的变量。因此就需要一个机制来让我们可以在函数体内部获取当前的运行环境，这便是this。
因此要明白 this 指向，其实就是要搞清楚 函数的运行环境**

## 全局作用域下的 this

全局作用域：非严格模式-浏览器环境下的 this 指向 window，严格模式-undefind
宿主环境根对象 DOM、BOM（browser object model）

```js
console.log(this) // window对象 Window {window: Window, self: Window, document: document, name: '', location: Location, …}

function test() {
    console.log(this) //window
}
test()
```

## 对象中的 this

在 JavaScript 中，一个对象可以包含多个方法。如果在对象的方法中使用 this，它的指向就是该对象本身。

```js
var obj = {
  name: 'obj',
  getName: function () {
    console.log(this.name)
  }
}
obj.getName() // obj
```

obj调用了getName方法，所以getName方法中的this指向obj

## 局部作用域下的 this

### 普通函数中的 this

```js
var name = '李四';
var obj = {
    name: '张三',
    fn: function () {
        console.log(this.name);//张三
        function fn2() {
            console.log(this.name);//李四
        }
        fn2();
        const fn3 = () => {
            console.log(this.name);//张三
        }
        fn3();
    }
}
obj.fn();
```

### 构造函数中的 this

如果是 new 绑定，并且构造函数中没有返回 object，那么 this 指向构造对象

```js
function Person() {
    this.name = '张三';
}

let p1 = new Person('Tom', 25);
console.log(p1); // Person {name: "张三"}
```

如果是 new 绑定，并且构造函数中返回 object，那么 this 指向返回的对象

```js
function Person(name, age) {
    this.name = '张三';
    return {
      name:'李四'
    }
}

let p1 = new Person('Tom', 25);
console.log(p1); // {name: '李四'}
```

## 在很多开发封装过程中（比如jquery、动画库jquery、gsap），this通常可以作为返回值实现链式调用

封装框架的时候要时刻注意this、因此常封装为一个context对象

```html
<div class="nihao">你好啊</div>
<script>
    function $(selector) {
        var doms = Array.prototype.slice.call(document.querySelectorAll(selector));
        return {
            css: function (styles) {
                Object.entries(styles).forEach(([key, value]) => {
                    doms.forEach(dom => {
                        dom.style[key] = value;
                    })
                })
                return this;
            },
            on: function (eventType, fn) {
                doms.forEach(dom => {
                    dom.addEventListener(eventType, fn);
                })
                return this
            },
        }
    }
    $('.nihao').css({
        color: 'red',
        fontSize: '30px'
    }).on('click', function () {
        alert('你好啊')
    })
</script>
```

## 练习题

### 题目1

```js
var obj = {
  foo: function () { console.log(this.bar) },
  bar: 1
};

var foo = obj.foo;
var bar = 2;

obj.foo() // 1
foo() // 2
```

虽然obj.foo和foo指向同一个函数，但是执行结果可能不一样。这种差异的原因，就在于函数体内部使用了this关键字。this指的是函数运行时所在的环境。对于obj.foo()来说，foo运行在obj环境，所以this指向obj；对于foo()来说，foo运行在全局环境，所以this指向全局环境。

原理参考：[https://www.ruanyifeng.com/blog/2018/06/javascript-this.html](https://www.ruanyifeng.com/blog/2018/06/javascript-this.html)

### 题目2

```js
function foo() {
  console.log(this.a)
}
var a = 1
foo() //1
const obj = {
  a: 2,
  foo: foo
}
obj.foo()//2
const c = new foo() //undefined
```

### 题目3

```js
function a() {
  return () => {
    return () => {
      console.log(this) // window
    }
  }
}
console.log(a()()())
```

首先箭头函数其实是没有 this 的，箭头函数中的 this 只取决包裹箭头函数的第一个普通函数的 this。在这个例子中，因为包裹箭头函数的第一个普通函数是 a，所以此时的 this 是 window

### 题目4

```js
var name='张三'
var a={
  name:'李四'
}
var fn=function(){
  console.log(this.name)
}
fn.call(a)  //李四
```

### 题目5

```js
var a = {}
var fn = function () { console.log(this) }
fn.bind().call(a)  //window
```

不管我们给函数 bind 几次，fn 中的 this 永远由第一次 bind 决定，所以结果永远是 window  call和apply也是同样（硬绑定）

参考链接：[https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/this](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Operators/this)

## 总结

判断this可以按照下面顺序优先级来判断：

1. 函数是否在new中调用（new绑定）？如果是的话this绑定的是新创建的对象。
2. 函数是否通过call、apply（显式绑定）或者硬绑定调用？如果是的话，this绑定的是指定的对象。
3. 函数是否在某个上下文对象中调用（隐式绑定）？如果是的话，this绑定的是那个上下文对象。
4. 如果都不是的话，使用默认绑定。如果在严格模式下，就绑定到undefined，否则绑定到全局对象。
