# 类型及检测方式

- js有几种数据类型，分别是什么？
- JavaScript 中的数据是如何存储在内存中的？
- 如何准确判断一个变量类型？如果让你写一个通用方法怎么实现？
- const声明的空数组能否push？

## 数据类型

JavaScript一共有8种数据类型，其中有7种基本数据类型：

1. Undefined
2. Null、
3. Boolean
4. Number
5. String
6. Symbol（es6新增，表示独一无二的值）
7. BigInt（es10新增）；

引用数据类型: 对象Object

1. 普通对象-Object，
2. 数组对象-Array，
3. 正则对象-RegExp，
4. 日期对象-Date，
5. 数学函数-Math，
6. 函数对象-Function

注意：**JavaScript不支持任何创建自定义类型的机制，而所有值最终都将是上述 8 种数据类型之一**

## JavaScript 中的数据是如何存储在内存中的

原始数据类型：
基础类型存储在栈内存，被引用或拷贝时，会创建一个完全相等的变量；占据空间小、大小固定，属于被频繁使用数据，所以放入栈中存储。

引用数据类型：
引用类型存储在堆内存，存储的是地址，多个引用指向同一个地址，这里会涉及一个“共享”的概念；占据空间大、大小不固定。引用数据类型在栈中存储了指针，该指针指向堆中该实体的起始地址。当解释器寻找引用值时，会首先检索其在栈中的地址，取得地址后从堆中获得实体。
**在 JavaScript 中，基本类型的赋值会完整复制变量值，而引用类型的赋值是复制栈内存中地址的引用。**

在 JavaScript 的执行过程中， 主要有三种类型内存空间，分别是代码空间、栈空间、堆空间。其中的代码空间主要是存储可执行代码的，基本类型(Number、String、Null、Undefined、Boolean、Symbol、BigInt)的数据值都是直接保存在“栈”中的，引用类型(Object)的值是存放在“堆”中的。因此在栈空间中(执行上下文)，基本类型存储的是变量的值，而引用类型存储的是其在"堆空间"中的，当 JavaScript 需要访问该数据的时候，是通过栈中的引用地址来访问的，相当于多了一道转手流程。

JavaScript 引擎需要用栈来维护程序执行期间上下文的状态，如果栈空间大了话，所有的数据都存放在栈空间里面，那么会影响到上下文切换的效率，进而又影响到整个程序的执行效率。通常情况下，栈空间都不会设置太大，主要用来存放一些基本类型的小数据。而引用类型的数据占用的空间都比较大，所以这一类数据会被存放到堆中，堆空间很大，能存放很多大的数据，不过缺点是分配内存和回收内存都会占用一定的时间。因此需要“栈”和“堆”两种空间。

## 练习题

### 题目1

```js
let a = {
  name: '张三',
  age: 18
}
let b = a;
console.log(a.name);  //张三
b.name = '李四';
console.log(a.name);  //李四
console.log(b.name);  //李四
```

### 题目2

```js
let a = {
  name: '张三',
  age: 16
}
function change(o) {
  o.age = 24;
  o = {
    name: '李四',
    age: 18
  }
  return o;
}
let b = change(a);
console.log(b.age);    // 18
console.log(a.age);    // 24
```

### const声明的空数组能否push操作？

可以，push 方法相当于是改变了堆内存中的数据结构。
const 只能保证栈内存中的地址不变，但是堆内存中的数据如何改变是没有办法控制的。

```js
const arr = [];
arr.push(1);
console.log(arr);   // [1]
```

## 数据类型检测

### typeof

1. 有更大的包容性 比如typeof a 为定义不会报错
2. 对于原始类型来说，除了 null 都可以显示正确的类型
3. 对于对象来说，除了函数都会显示 object，所以说 typeof 并不能准确判断变量到底是什么类型。typeof null为object 原因是对象存在在计算机中，都是以000开始的二进制存储，所以检测出来的结果是对象

```js
console.log(typeof 2);               // number
console.log(typeof true);            // boolean
console.log(typeof 'str');           // string
console.log(typeof []);              // object     []数组的数据类型在 typeof 中被解释为 object
console.log(typeof function(){});    // function
console.log(typeof {});              // object
console.log(typeof undefined);       // undefined
console.log(typeof null);            // object     null 的数据类型被 typeof 解释为 object
```

### instanceof

1. 可以正确的判断对象的类型，因为内部机制是通过判断对象的原型链中是不是能找到类型的 prototype
2. 不能正确判断基础数据类型；

```js
console.log(2 instanceof Number);                    // false
console.log(true instanceof Boolean);                // false 
console.log('str' instanceof String);                // false  
console.log([] instanceof Array);                    // true
console.log(function(){} instanceof Function);       // true
console.log({} instanceof Object);                   // true 
```

### constructor

这里有一个坑，如果我创建一个对象，更改它的原型，constructor就会变得不可靠了，因为它会指向原型链中的第一个原型的 constructor

```js
console.log((2).constructor === Number); // true
console.log((true).constructor === Boolean); // true
console.log(('str').constructor === String); // true
console.log(([]).constructor === Array); // true
console.log((function() {}).constructor === Function); // true
console.log(({}).constructor === Object); // true
//下面是修改了Fn的原型
function Fn(){};
Fn.prototype=new Array();
var f=new Fn();
console.log(f.constructor===Fn);    // false
console.log(f.constructor===Array); // true
```

### Object.prototype.toString.call()

toString() 是 Object 的原型方法，调用该方法，可以统一返回格式为 “[object Xxx]” 的字符串，其中 Xxx 就是对象的类型。对于 Object 对象，直接调用 toString() 就能返回 [object Object]；而对于其他对象，则需要通过 call 来调用，才能返回正确的类型信息。

```js
Object.prototype.toString.call(1)    // "[object Number]"
Object.prototype.toString.call('1')  // "[object String]"
Object.prototype.toString.call(true)  // "[object Boolean]"
Object.prototype.toString.call(null)   //"[object Null]"
Object.prototype.toString.call(undefined) //"[object Undefined]"
Object.prototype.toString.call({})  // "[object Object]"
Object.prototype.toString.call([])       //"[object Array]"
Object.prototype.toString.call(function(){})  // "[object Function]"
Object.prototype.toString.call(/123/g)    //"[object RegExp]"
Object.prototype.toString.call(new Date()) //"[object Date]"
Object.prototype.toString.call(document)  //"[object HTMLDocument]"
Object.prototype.toString.call(window)   //"[object Window]"
```

### 实现一个全局通用的数据类型判断方法

```js
function getType(obj){//需要注意大小写，哪些是typeof判断
  let type  = typeof obj;
  if (type !== "object") {    // 先进行typeof判断，如果是基础数据类型，直接返回
    return type;
  }
  // 对于typeof返回结果是object的，再进行如下的判断，正则返回结果
  return Object.prototype.toString.call(obj).replace(/^\[object (\S+)\]$/, '$1');  // 注意正则中间有个空格
}
```
