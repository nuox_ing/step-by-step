# null和undefined

## question

- null和undefined的区别是什么？

## null、undefined

### 相同点

null和undefined都只有一个值，即他们的名称即是类型也是值。

### 不同点

null 指空值，或者说是“非值”，它是一个特殊的关键字，不是标识符，所以不能用作变量名或者函数名。
undefined 指从未赋值 ，它是一个标识符，而不是关键字，所以可以用作变量名或者函数名。

```js
//全局undefined赋值
function foo() {
    undefined = 2
    console.log( undefined )
}
foo() //undefined//严格模式下TypeError: Cannot assign to read only property 'undefined' of object '#<Window>'

//局部undefined赋值
function foo() {
    "use strict"
    var undefined = 2
    console.log( undefined )
}
foo() //2
```
