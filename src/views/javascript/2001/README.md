# 1loc Object

```js
/**
1. 检查多个对象是否相等
isEqual({ foo: 'bar' }, { foo: 'bar' }, { foo: 'bar' })//true
isEqual({ foo: 'bar' }, { bar: 'foo' }) // false
*/

/**
2. 创建一个没有原型的空对象
console.log(map)//{}
 */

/**
3. 用键和值对创建一个对象
toObj([
    ['a', 1],
    ['b', 2],
    ['c', 3],
]) // { a: 1, b: 2, c: 3 }
 */

/**
4. 从对象数组中提取属性值
pluck(
    [
        { name: 'John', age: 20 },
        { name: 'Smith', age: 25 },
        { name: 'Peter', age: 30 },
    ],
    'name'
)// ['John', 'Smith', 'Peter']
 */

/**
5. 获取对象给定路径处的值
getValue('a.b', { a: { b: 'Hello World' } })// Hello World
 */

/**
6. 按键返回具有唯一值的对象
getUniqueArrObj(
    [{ k: 1, e: 1 }, { k: 1, e: 1 }, { k: 3, e: 1 }],
    'k'
)// [{ k: 1, e: 1 }, { k: 3, e: 1 }]
 */

/**
7. 不可变地重命名对象键
const obj = { a: 1, b: 2, c: 3 };
const keysMap = { a: 'd', b: 'e', c: 'f' };
console.log(renameKeys(keysMap, obj)) // { d: 1, e: 2, f: 3 }
 */

/**
8. 反转对象的键和值
console.log(invert({ a: '1', b: '2', c: '3' })) //{1: 'a', 2: 'b', 3: 'c'}
 */

/**
9. 省略对象的属性子集
console.log(omit({ a: '1', b: '2', c: '3' }, ['a', 'b'])) //{ c: '3' }
 */

/**
10. 选择对象属性的子集
console.log(pick({ a: '1', b: '2', c: '3' }, ['a', 'b'])) //{a: '1', b: '2'}
 */

/**
11. 移除对象中null和undefined的属性
console.log(removeNullUndefined({ foo: null, bar: undefined, fuzz: 42 }))// { fuzz: 42 }
 */

/**
12. 按对象的属性对对象进行排序
 const colors = {
    white: '#ffffff',
    black: '#000000',
    red: '#ff0000',
    green: '#008000',
    blue: '#0000ff',
};
console.log(sortC(colors))
{
    black: '#000000',
    blue: '#0000ff',
    green: '#008000',
    red: '#ff0000',
    white: '#ffffff',
}
 */
```

## 1 检查多个对象是否相等

```js
const isEqual=(...objects)=>objects.every(obj=>JSON.stringify(obj)===JSON.stringify(objects[0]))
console.log(isEqual({ foo: 'bar' }, { foo: 'bar' }, { foo: 'bar' })) // true
console.log(isEqual({ foo: 'bar' }, { bar: 'foo' })) // false
```

## 2 创建一个没有原型的空对象

```js
const map=Object.create(null)
console.log(map)//{}
```

## 3 用键和值对创建一个对象

```js
// const toObj = arr => Object.fromEntries(arr)
const toObj = arr => arr.reduce((a, c) => ((a[c[0]] = c[1]),a), {})
console.log(toObj([
    ['a', 1],
    ['b', 2],
    ['c', 3],
])) // { a: 1, b: 2, c: 3 }
```

## 4 从对象数组中提取属性值

```js
const pluck=(arr,name)=>arr.map(item=>item[name])
console.log(pluck(
    [
        { name: 'John', age: 20 },
        { name: 'Smith', age: 25 },
        { name: 'Peter', age: 30 },
    ],
    'name'
))// ['John', 'Smith', 'Peter']
```

## 5 获取对象给定路径处的值

```js
const getValue = (path, obj) => path.split('.').reduce((a, c) => (a && a[c]), obj)
console.log(getValue('a.b', { a: { b: 'Hello World' } }))
```

## 6 按键返回具有唯一值的对象

```js
const getUniqueArrObj = (arr, k) => Array.from(new Map(arr.map(item=>[item[k],item])).values())
// const getUniqueArrObj = (arrObj, keyUnique) => [...new Map(arrObj.map((item) => [item[keyUnique], item])).values()];
console.log(
    getUniqueArrObj(
        [{ k: 1, e: 1 }, { k: 1, e: 1 }, { k: 3, e: 1 }],
        'k'
    )
)// [{ k: 1, e: 1 }, { k: 3, e: 1 }]
```

## 7 不可变地重命名对象键

```js
// const renameKeys = (keysMap, obj) => Object.keys(obj).reduce((acc, key) => ({ ...acc, ...{ [keysMap[key] || key]: obj[key] } }), {});
const renameKeys = (keysMap, obj) => Object.keys(keysMap).reduce((a, c) => (a[keysMap[c] || c] = obj[c], a), {})
const obj = { a: 1, b: 2, c: 3 };
const keysMap = { a: 'd', b: 'e', c: 'f' };
console.log(renameKeys(keysMap, obj)) // { d: 1, e: 2, f: 3 }
```

## 8 反转对象的键和值

```js
// const invert = (obj) => Object.keys(obj).reduce((a, c) => (a[obj[c]] = c,a), {})
const invert = (obj) => Object.fromEntries(Object.entries(obj).map(([k, v]) => [v, k]))
console.log(invert({ a: '1', b: '2', c: '3' })) //{1: 'a', 2: 'b', 3: 'c'}
```

## 9 省略对象的属性子集

```js
// const omit = (obj, arr) => Object.keys(obj).filter(item => !arr.includes(item)).reduce((a, c) => (a[c] = obj[c],a), {})
const omit = (obj, keys) => Object.keys(obj)
    .filter((k) => !keys.includes(k))
    .reduce((res, k) => Object.assign(res, { [k]: obj[k] }), {});
console.log(omit({ a: '1', b: '2', c: '3' }, ['a', 'b'])) //{ c: '3' }
```

## 10 选择对象属性的子集

```js
// const pick = (obj, arr) => Object.keys(obj).filter(item => arr.includes(item)).reduce((a, c) => (a[c] = obj[c],a), {})
const pick = (obj, keys) => Object.keys(obj)
    .filter((k) => keys.includes(k))
    .reduce((res, k) => Object.assign(res, { [k]: obj[k] }), {});
console.log(pick({ a: '1', b: '2', c: '3' }, ['a', 'b'])) //{a: '1', b: '2'}
```

## 11 移除对象中null和undefined的属性

```js
// const removeNullUndefined = (obj) => Object.entries(obj).reduce((a, [k, v]) => ((v !== null && v !== undefined) ? a[k] = v : null, a), {})
const removeNullUndefined = (obj) => Object.fromEntries(Object.entries(obj).filter(([_, v]) => v !== null&&v !== undefined))
console.log(removeNullUndefined({ foo: null, bar: undefined, fuzz: 42 }))
```

## 12 按对象的属性对对象进行排序

```js
const sortC = (obj)=>Object.keys(obj).sort().reduce((a, c) => (a[c] = obj[c], a), {})
const colors = {
    white: '#ffffff',
    black: '#000000',
    red: '#ff0000',
    green: '#008000',
    blue: '#0000ff',
};
console.log(sortC(colors))
/*
{
    black: '#000000',
    blue: '#0000ff',
    green: '#008000',
    red: '#ff0000',
    white: '#ffffff',
}
*/
```
