# 继承

## question

- 说下原型继承
- 怎么理解prototype、__proto__、constructor

继承是指子类继承父类的属性和方法，并可以在此基础上添加新的属性和方法。继承使得代码的复用和扩展变得更加容易。
面向对象之继承相较于面向函数之组合（lodash、ramda）

## __proto__

proto 是 JavaScript 中每个对象都有的属性，它是一个指向该对象原型的指针。每个 JavaScript 对象都有一个原型，也就是该对象从其父对象继承属性和方法。在创建对象时，__proto__ 会被初始化为其构造函数的原型对象，它可以指向另一个对象，也可以是 null。

```js
let person = {};
console.log(person.__proto__ === Object.prototype); // true
```

## 原型如何实现继承？Class 如何实现继承？Class 本质是什么？

class，其实在 JS中并不存在类，class 只是语法糖，本质还是函数

## 原型链实现继承 Child.prototype=new Parent()

```js
function Parent(){
    this.name='张三',
    this.like=['看美女']
    this.sports=function(){
        console.log(this.name)
    }
}
Parent.prototype.getName=function(){
    console.log(`Parent.prototype${this.name}`)
}
function Child(){}
Child.prototype=new Parent()//new的是单列
//测试
const test1=new Child()
test1.name='你好'
test1.like.push('吃饭')
console.log(test1.name) //你好
console.log(test1.like) //['看美女', '吃饭']
test1.sports()  //你好
test1.getName() //Parent.prototype你好

const test2=new Child()
console.log(test2.name) //张三
console.log(test2.like) //['看美女', '吃饭']
test2.sports()  //张三
test2.getName() //Parent.prototype张三
```

优点：父类方法可以复用
缺点：父类引用类型会被共享，指向同一个内存空间。
子类实例不能给父类传产

## 构造函数实现继承 Parent.call(this)

```js
    function Parent(){
      this.name='张三',
      this.like=['看美女']
      this.sports=function(){
        console.log(this.name)
      }
    }
    Parent.prototype.getName=function(){
      console.log(`Parent.prototype${this.name}`)
    }
    function Child(){
      Parent.call(this)
    }
    //测试
    const test1=new Child()
    test1.name='你好'
    test1.like.push('吃饭')
    console.log(test1.name)
    console.log(test1.like)
    test1.sports()
    // test1.getName()  //不能访问父类原型属性上的参数和方法

    const test2=new Child()
    console.log(test2.name)
    console.log(test2.like)
    test2.sports()  //不能访问父类原型属性上的参数和方法
    // test2.getName()
```

优点：父类引用类型不会被共享
缺点：不能访问父类原型属性上的参数和方法

## 组合继承 Parent.call(this)+Child.prototype=new Parent()

```js
    function Parent(){
      this.name='张三',
      this.like=['看美女']
      this.sports=function(){
        console.log(this.name)
      }
    }
    Parent.prototype.getName=function(){
      console.log(`Parent.prototype${this.name}`)
    }
    function Child(){
      Parent.call(this)
    }
    Child.prototype=new Parent()
    //测试
    const test1=new Child()
    test1.name='你好'
    test1.like.push('吃饭')
    console.log(test1.name)
    console.log(test1.like)
    test1.sports()
    test1.getName() 

    const test2=new Child()
    console.log(test2.name)
    console.log(test2.like)
    test2.sports()  
    test2.getName()
```

优点：父类引用类型不会被共享,可以访问父类原型属性上的参数和方法
缺点：会调用两次父类的构造函数，会有两份一样的属性和方法，会影响性能

## 寄生组合继承 不使用Object.create

```js
    function Parent(){
      this.name='张三',
      this.like=['看美女']
      this.sports=function(){
        console.log(this.name)
      }
    }
    Parent.prototype.getName=function(){
      console.log(`Parent.prototype${this.name}`)
    }
    function Child(){
      Parent.call(this)
    }
    const Fn=function(){}
    Fn.prototype=Parent.prototype;
    Child.prototype=new Fn();
    //测试
    const test1=new Child()
    test1.name='你好'
    test1.like.push('吃饭')
    console.log(test1.name)
    console.log(test1.like)
    test1.sports()
    test1.getName() 

    const test2=new Child()
    console.log(test2.name)
    console.log(test2.like)
    test2.sports()  
    test2.getName()
```

## 寄生组合继承 使用Object.create

```js
    function Parent(){
      this.name='张三',
      this.like=['看美女']
      this.sports=function(){
        console.log(this.name)
      }
    }
    Parent.prototype.getName=function(){
      console.log(`Parent.prototype${this.name}`)
    }
    function Child(){
      Parent.call(this)
    }
    Child.prototype=Object.create(Parent.prototype)
    //测试
    const test1=new Child()
    test1.name='你好'
    test1.like.push('吃饭')
    console.log(test1.name)
    console.log(test1.like)
    test1.sports()
    test1.getName() 

    const test2=new Child()
    console.log(test2.name)
    console.log(test2.like)
    test2.sports()  
    test2.getName()
```

## ES6 class extends继承
