# Set

## question

- Set是什么有哪些方法
- Set和Map区别
- 什么时候用到Set
- 数组去重复

## 简述

对象允许你存储任何类型的唯一值，无论是原始值或者是对象引用。
Set对象是值的集合，你可以按照插入的顺序迭代它的元素。Set 中的元素只会出现一次，即 Set 中的元素是唯一的。

## 属性Set.prototype

size 返回 Set 对象中（唯一的）元素的个数。

```js
const set = new Set([0, 1, 2, 2])
console.log(set.size)//3
```

## 方法Set.prototype

1. add() 方法将插入一个具有指定值的新元素到 Set 对象中
2. clear() 方法移除 Set 对象中所有元素
3. delete() 方法从 Set 对象中删除指定的值（如果该值在 Set 中）。
4. entries() 方法返回一个新的迭代器对象，由于集合对象不像 Map 对象那样拥有 key，然而，为了与 Map 对象的 API 形式保持一致，故使得每一个条目的 key 和 value 都拥有相同的值，因而最终返回一个 [value, value] 形式的数组。
5. forEach() 方法对 Set 对象中的每个值按插入顺序执行一次提供的函数。forEach(callbackFn(value,key,Set)=>{}, thisArg)
6. has() 方法返回一个布尔值来指示对应的值value是否存在Set对象中。
7. keys() 方法返回一个新的迭代器对象，该对象包含按照原始 Set 对象中元素插入顺序排列的所有元素的值。
8. values() 方法返回一个新的迭代器对象，该对象包含按照原始 Set 对象中元素插入顺序排列的所有元素的值。

```js
const set = new Set()
const obj = { a: 1 }
set.add(1).add(2).add(true)
set.add(obj)
set.add(obj)//同一个引用地址，不会添加
set.add({ a: 1 })//不同的引用地址会被添加
console.dir(set.size)//4

set.delete(1)//删除基本数据类型
set.forEach(val => val.a === 1 && set.delete(val))//删除引用数据类型

console.log(set.has(true))//true
console.log(set.entries())//SetIterator {2 => 2, true => true}
console.log(set.keys())//SetIterator {2, true}
console.log(set.values())//SetIterator {2, true}

let a=set.entries()
console.log(a.next().value)// [2, 2]
console.log(a.next().value)// [true, true]
console.log(a.next().value)// undefined

set.clear()
console.log(set)//Set(0) {size: 0}
```

## 数组相关

### Set和Array互转

```js
let arr=[1,2,3]
let set=new Set(arr)
console.log(set)//Set(3) {1, 2, 3}
let arr2=Array.from(set)
console.log(arr2)//[1, 2, 3]
let arr3=[...set]
console.log(arr3)//[1, 2, 3]
```

### 数组去重

```js
let arr=[1,2,2,5]
let newArr=Array.from(new Set(arr))
console.log(newArr)//[1, 2, 5]
```