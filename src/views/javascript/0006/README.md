# 函数科里化

## question

- 什么是函数科里化
- 说下函数科里化的应用场景
- 科里化的实现原理，写一个通用的科里化函数

## 什么是科里化

柯里化（Currying）是一种关于函数的高阶技术。它不仅被用于 JavaScript，还被用于其他编程语言。
柯里化是一种函数的转换，它是指将一个函数从可调用的 f(a, b, c) 转换为可调用的 f(a)(b)(c)。
柯里化不会调用函数。它只是对函数进行转换。

## 科里化应用场景

### 场景一 减少重复传递不变的部分参数

```js
//比如将三个参数 http www.yerjs.com index.html 合成url地址
//http://www.yerjs.com/index.html
//可以这么实现
function simpleURL(protocol, domain, path) {
    return protocol + "://" + domain + "/" + path;
}
simpleURL('http', 'www.yerjs.com', 'index.html')
simpleURL('http', 'www.yerjs.com', 'about.html')
simpleURL('http', 'www.yerjs.com', 'home.html')
//如果跟多呢？每次都要传递相同的参数，这样就很麻烦了
//避免重复传递相同的参数，可以使用科里化 
//可以使用lodash的curry函数(https://github.com/lodash/lodash)
const url=_.curry(simpleURL)('http', 'www.yerjs.com')
const res1=url('index.html')
const res2=url('about.html')
const res3=url('home.html')
```

### 场景二 将柯里化后的callback参数传递给map, filter等函数

比如我们有这样一段数据：

```js
const datas=[
  {
    id:1,
    xdata:'2020',
    ydata:100
  },
  {
    id:1,
    xdata:'2021',
    ydata:102
  },
  {
    id:1,
    xdata:'2020',
    ydata:103
  }
]
```

怎么分别取出xdata和ydata的值呢？

```js
const xdata=datas.map(item=>item.xdata)
const ydata=datas.map(item=>item.ydata)
```

这样写是可以的，但是有重复的代码，可以使用科里化来优化

```js
const datas=[
  {
    id:1,
    xdata:'2020',
    ydata:100
  },
  {
    id:1,
    xdata:'2021',
    ydata:102
  },
  {
    id:1,
    xdata:'2020',
    ydata:103
  }
]
const c=curry((key,obj)=>obj[key])
console.log(datas.map(c('xdata')))
console.log(datas.map(c('ydata')))
```

## 实现curry函数

```js
function curry(fn) {
    return function curried(...args) {
        if (args.length >= fn.length) {
            return fn.apply(this, args);
        } else {
            return function (...args2) {
                return curried.apply(this, args.concat(args2));
            }
        }
    };
}
```
