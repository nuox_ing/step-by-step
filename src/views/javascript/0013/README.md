# Object

## question

- Object有哪些方法
- 怎么声明一个没有原型的空对象

## object声明

Object 是 JavaScript 的一种数据类型。它用于存储各种键值集合和更复杂的实体。可以通过 Object() 构造函数或者使用对象字面量的方式创建对象。

1. 构造函数声明 var obj=new Object({a:1})
2. 对象字面量声明 var obj={a:1}

## 对象的赋值

1. 如果对象中存在该属性，则覆盖该属性的值
2. 如果对象中存在该属性，原型链中也存在该属性，则覆盖原型链中的属性值
3. 如果对象中不存在该属性，原型链中也不存在该属性，则给对象添加该属性
4. 如果对象中不存在该属性，原型链中存在该属性(并没有标记为只读writable:false),那么就回直接添加一个新属性
5. 如果对象中不存在该属性，原型链中存在该属性(标记为只读writable:false)严格模式会抛出错误，非严格模式下不会报错，赋值会被忽略
6. 如果对象中不存在该属性，原型链中存在该属性(并且它是一个setter)，那么会调用setter,不会添加属性到对象中，也不会重新定义foo这个setter

```js
//4
let a = Object.defineProperty({}, 'b', {
    value: 1,
    writable: true
})
let b = Object.create(a)
b.b = 2
console.log(b)//{b: 2}
//5
let a = Object.defineProperty({}, 'b', {
    value: 1,
    writable: false
})
let b = Object.create(a)
b.b = 2
console.log(b)//{}
//6
let a = Object.defineProperty({}, 'b', {
    set: function (value) {
        this.c = value
    }
})
let b = Object.create(a)
b.b = 2
console.log(b)//{c: 2}
```

## Object方法

### Object.assign(target, ...sources)

静态方法将一个或者多个源对象中所有可枚举的自有属性复制到目标对象，并返回修改后的目标对象。

```js
var obj1={a:1}
var obj2={b:2}
var obj3={c:3}
let obj=Object.assign(obj1,obj2,obj3)
console.log(obj)//{a: 1, b: 2, c: 3}
console.log(obj1)//{a: 1, b: 2, c: 3}
console.log(obj1==obj)//true
```

### Object.create(proto, props)

静态方法以一个现有对象作为原型，创建一个新对象。

props

1. configurable 如果此属性描述符的类型可以更改并且属性可以从相应的对象中删除，则为 true。默认为 false。
2. enumerable 如果此属性可以在枚举期间显示在对象属性的枚举中，则为 true。默认为 false。
3. value 与属性关联的值。可以是任何有效的 JavaScript 值（数字，对象，函数等）。默认为 undefined。
4. writable 如果属性的值可以被修改，则为 true。默认为 false。
5. get 一个给属性提供 getter 的方法，如果没有 getter 则为 undefined。该方法返回值被用作属性值。默认为 undefined。
6. set 一个给属性提供 setter 的方法，如果没有 setter 则为 undefined。该方法将接受唯一参数，并将该参数的新值分配给该属性。默认为 undefined。

```js
var obj1 = { a: 1 }
obj1.say = function () {
    console.log('我是say方法')
}
var obj2 = Object.create(obj1)
obj2.say()//我是say方法

var obj3=Object.create(obj1,{
    b:{
        value:2,
        writable:true,
        configurable:true,
        enumerable:true
    }
})

var obj4=Object.create(null)//创建一个没有原型的空对象
```

### Object.defineProperties(obj, props)

静态方法直接在一个对象上定义新的属性或修改现有属性，并返回该对象

```js
var obj1 = { a: 1 }
obj1.say = function () {
    console.log('我是say方法')
}
var obj2 = Object.defineProperties(obj1,{b:{value:2}})
console.log(obj2)//{a: 1, b: 2, say: ƒ}
obj2.say()//我是say方法
```

### Object.defineProperty(obj, prop, descriptor)

静态方法会直接在一个对象上定义一个新属性，或者修改一个对象的现有属性，并返回此对象。

```js
var obj1 = { a: 1 }
obj1.say = function () {
    console.log('我是say方法')
}
var obj2 = Object.defineProperty(obj1,'b',{value:2})
console.log(obj2)//{a: 1, b: 2, say: ƒ}
obj2.say()//我是say方法
```

### Object.entries(obj)

返回一个数组，包含给定对象自有的可枚举字符串键属性的键值对。

### Object.keys(obj)

返回一个由给定对象自身的可枚举的字符串键属性名组成的数组。

### Object.values(obj)

返回一个给定对象自身的所有可枚举属性值的数组。

### Object.getOwnPropertyNames(obj)

静态方法返回一个数组，其包含给定对象中所有自有属性（包括不可枚举属性，但不包括使用 symbol 值作为名称的属性）

### Object.getOwnPropertySymbols(obj)

静态方法返回一个包含给定对象所有自有 Symbol 属性的数组

```js
var obj = { a: 1, b: 2, c: 3 }
var sm = Symbol('e')
Object.defineProperties(obj, { d: { value: 4, enumerable: false } })
obj[sm] = 5
console.log(obj)//{a: 1, b: 2, c: 3, d: 4, Symbol(e): 5}
console.log(Object.keys(obj))//['a', 'b', 'c']
console.log(Object.values(obj))//[1, 2, 3]
console.log(Object.entries(obj))////[['a', 1],['b', 2],['c', 3]]
console.log(Object.getOwnPropertyNames(obj))//['a', 'b', 'c', 'd']
console.log(Object.getOwnPropertySymbols(obj))//[Symbol(e)]
```

### Object.freeze(obj)

冻结一个对象。一个被冻结的对象再也不能被修改；冻结了一个对象则不能向这个对象添加新的属性，不能删除已有属性，不能修改该对象已有属性的可枚举性、可配置性、可写性，以及不能修改已有属性的值。此外，冻结一个对象后该对象的原型也不能被修改。freeze() 返回和传入的参数相同的对象。

### Object.isFrozen(obj)

静态方法判断一个对象是否被冻结

```js
var obj={a:1}
console.log(Object.isFrozen(obj))//true
Object.freeze(obj)
obj.a=2//严格模式下会报错
console.log(obj)//{a: 1}
console.log(Object.isFrozen(obj))//false
```

### Object.fromEntries(iterable)

静态方法 Object.fromEntries() 把键值对列表转换为一个对象。

```js
var arr = [['a', 1], ['b', 2]]
var map = new Map(arr)
console.log(map)//Map(2) {'a' => 1, 'b' => 2}
var obj2 = Object.fromEntries(map)
console.log(obj2)//{a: 1, b: 2}

console.log(Object.fromEntries(arr))//{a: 1, b: 2}
```

### Object.getOwnPropertyDescriptor(obj, prop)

返回指定对象上一个自有属性对应的属性描述符。
如果指定的属性存在于对象上，则返回其属性描述符，否则返回 undefined

```js
var obj={a:1}
var descriptor=Object.getOwnPropertyDescriptor(obj,'a')
console.log(descriptor)//{value: 1, writable: true, enumerable: true, configurable: true}
console.log(descriptor.value)//1
```

### Object.getOwnPropertyDescriptors(obj)

```js
var obj={a:1,b:2}
var descriptor=Object.getOwnPropertyDescriptors(obj)
console.log(descriptor)//[{a:{value: 1, writable: true, enumerable: true, configurable: true}},{b:{value: 2, writable: true, enumerable: true, configurable: true}}]
```

### Object.getPrototypeOf(obj)

返回指定对象的原型（即内部 [Prototype] 属性的值） 或者null

```js
var obj = { a: 1, b: 2, c: 3 }
var obj2=Object.create(obj)
console.log(Object.getPrototypeOf(obj2))//{a: 1, b: 2, c: 3}
console.log(Object.getPrototypeOf(obj2)==obj)//true
```

### Object.hasOwn(obj, prop)

如果指定的对象自身有指定的属性，则静态方法 Object.hasOwn() 返回 true。如果属性是继承的或者不存在，该方法返回 false
prop:要测试属性的 String 类型的名称或者 Symbol

Object.hasOwn() 旨在取代 Object.prototype.hasOwnProperty()

```js
var obj = { a: 1, b: 2, c: 3 }
var obj2 = Object.create(obj, { d: { value: 4 } })
console.log(obj2)//{d: 4}
console.log(Object.hasOwn(obj2, 'd'))//true
console.log(Object.hasOwn(obj2, 'a'))//false
```

### Object.is(value1, value2)

确定两个值是否为相同值

```js
console.log(Object.is(-0,0))//false
console.log(Object.is(NaN,NaN))//true
console.log(-0===0)//true
console.log(NaN===NaN)//false
```

### Object.prototype.isPrototypeOf(object)

isPrototypeOf() 方法用于测试一个对象是否存在于另一个对象的原型链上

```js
var obj={a:1}
var obj2=Object.create(obj)
console.log(Object.prototype.isPrototypeOf(obj,obj2))//true
```

### Object.preventExtensions(obj)

静态方法可以防止新属性被添加到对象中（即防止该对象被扩展）。它还可以防止对象的原型被重新指定。

### Object.isExtensible(obj)

静态方法判断一个对象是否是可扩展的（是否可以在它上面添加新的属性）

```js
var obj={a:1}
console.log(Object.isExtensible(obj))//true
Object.preventExtensions(obj)
console.log(Object.isExtensible(obj))//false
```

### Object.seal(obj)

静态方法密封一个对象。密封一个对象会阻止其扩展并且使得现有属性不可配置。密封对象有一组固定的属性：不能添加新属性、不能删除现有属性或更改其可枚举性和可配置性、不能重新分配其原型。只要现有属性的值是可写的，它们仍然可以更改。seal() 返回传入的同一对象。

### Object.isSealed(obj)

静态方法判断一个对象是否被密封。

```js
var obj={a:1}
console.log(Object.isSealed(obj))//false
Object.seal(obj)
console.log(Object.isSealed(obj))//true
```

### Object.prototype.propertyIsEnumerable(prop)

```js
var obj={a:1}
Object.defineProperty(obj,'b',{
    value:2,
    enumerable:false
})
console.log(obj)
console.log(obj.propertyIsEnumerable('a'))//true
console.log(obj.propertyIsEnumerable('b'))//false
```

### Object.setPrototypeOf(obj, prototype)

静态方法设置一个指定的对象的原型 (即, 内部Prototype属性) 到另一个对象或  null。

```js
var obj={a:1}
var obj2={}
console.log(obj2.a)
Object.setPrototypeOf(obj2,obj)//undefind
console.log(obj2.a)//1
```

## Object.prototype.toString()

方法返回一个表示该对象的字符串。该方法旨在重写（自定义）派生类对象的类型转换的逻辑。

```js
var obj={a:1}
console.log(obj.toString())//[object Object]
```

## Object.prototype.valueOf()

方法将 this 值转换成对象。该方法旨在被派生对象重写，以实现自定义类型转换逻辑。
方法返回指定对象的原始值(this)。

```js
var obj={a:1}
console.log(obj)//{a:1}
var obj2={b:1}
console.log(obj.valueOf.call(obj2))//{b: 1}
console.log(obj.valueOf.call(this))//Window {window: Window, self: Window, document: document, name: '', location: Location, …}
```
