# 数据库表的相关操作

## question

- 怎么把树转为数组
- 怎么把数组转为树

## SQL中注释的写法

```sql
# 这是单行注释
/*这是多行注释 */
```

## 创建库相关

```sql
/*查询逻辑空间*/
SHOW DATABASES ;
/*创建逻辑空间*/
CREATE DATABASE demo;
/*删除逻辑空间*/
DROP DATABASE demo;
/*切换逻辑空间*/
USE demo;
```

## 表相关

### 创建表

CREATE TABLE 数据表名 (
    字段名1 数据类型 [ 约束 ] [ 默认值 ] [COMMENT 注释] ,
    ...
    字段名n 字段类型n
)[ COMMENT=注释 ];

```sql
CREATE TABLE student(
    id INT UNSIGNED PRIMARY KEY ,
    name VARCHAR(20) NOT NULL ,
    sex CHAR(1) NOT NULL ,
    birthday DATE NOT NULL ,
    tel CHAR(11) NOT NULL ,
    remark VARCHAR(200)
)COMMENT='学生表';
/*查看所有数据表*/
SHOW tables ;
/*查看表结构*/
DESC student;
/*查看创建时sql*/
SHOW CREATE TABLE student;
/*删除表*/
DROP TABLE student;

```

### 修改表
