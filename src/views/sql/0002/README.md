# 数据类型

## question

- 怎么把树转为数组
- 怎么把数组转为树

## 数字

|  类型   | 大小 bytes  |    范围（有符号）   |   范围（无符号）   | 用途  |
|  ----  | ----  |  ----  | ----  | ----  |
| TINYINT  | 1  | (-128，127)  | (0，255) | 小整数值 |
| SMALLINT  | 2  | (-32 768，32 767)  | (0，65 535) | 大整数值 |
|MEDIUMINT | 3  | (-8 388 608，8 388 607)| (0，16 777 215) |大整数值|
|INT或INTEGER |4  |(-2 147 483 648，2 147 483 647) |(0，4 294 967 295) |大整数值|
|BIGINT| 8 | (-9,223,372,036,854,775,808，9 223 372 036 854 775 807)| (0，18 446 744 073 709 551 615)| 极大整数值|
|FLOAT| 4 | | | 单精度浮点数值 |
|DOUBLE| 8  |||双精度浮点数值|
|DECIMAL| 对DECIMAL(M,D) ，如果M>D，为M+2否则为D+2 |依赖于M和D的值| 依赖于M和D的值| 小数值|

## 字符串

|类型|大小 bytes|用途|
|----|----|----|
|CHAR|0-255 |定长字符串|
|VARCHAR|0-65535 |变长字符串|
|TINYBLOB|0-255 |不超过 255 个字符的二进制字符串|
|TINYTEXT|0-255 |短文本字符串|
|BLOB|0-65 535 |二进制形式的长文本数据|
|TEXT|0-65 535 |长文本数据|
|MEDIUMBLOB|0-16 777 215 |二进制形式的中等长度文本数据|
|MEDIUMTEXT|0-16 777 215 |中等长度文本数据|
|LONGBLOB|0-4 294 967 295 |二进制形式的极大文本数据|
|LONGTEXT|0-4 294 967 295 |极大文本数据|

## 日期类型

|类型|大小 bytes|范围|格式|用途|
|----|----|----|----|----|
|DATE|3|1000-01-01/9999-12-31|YYYY-MM-DD|日期值|
|TIME|3|'-838:59:59'/'838:59:59'|HH:MM:SS|时间值或持续时间|
|YEAR|1|1901/2155|YYYY|年份值|
|DATETIME|8|'1000-01-01 00:00:00' 到 '9999-12-31 23:59:59'|YYYY-MM-DD hh:mm:ss|混合日期和时间值|
TIMESTAMP|4|'1970-01-01 00:00:01' UTC 到 '2038-01-19 03:14:07' UTC|YYYY-MM-DD hh:mm:ss|时间戳|
