# React和vue区别

## question

- 说下react和vue区别

## 相同点

- 虚拟dom
- 组件式开发

## 不同点

### 语法不同

- react使jsx语法
- vue 单文件组件SFC（使用 .vue 作为文件扩展名，它是一种使用了类似 HTML 语法的自定义文件格式，用于定义 Vue 组件。一个 Vue 单文件组件在语法上是兼容 HTML 的。每一个 .vue 文件都由三种顶层语言块构成：template、script 和 style，以及一些其他的自定义块）

### 数据是否可变

- react是immutable的。
- vue是mutable的（vue2通过Object.defineProtity、vue3通过Proxy）

### 数据流不同

- vue双向数据绑定
- react 单向数据流
