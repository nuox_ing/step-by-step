# useRef

## question

- useRef是用来解决什么问题的
- useRef的使用场景

## 语法

```js
const refContainer = useRef(initialValue);
```

## 什么是useRef

useRef返回一个可变的ref对象，该对象只有一个current属性，初始化值为传入的参数（initialValue），并且返回的ref对象在组件的整个生命周期内保存不变

## useRef是用来解决如下问题的

- 获取子组件或者DOM节点的实例对象；
- 储存渲染周期之间的共享数据。

## 使用场景

### 获取子DOM节点的实例

通过inputRef.current获取对应的dom对象

```js
import React, { useRef, useEffect } from 'react';
function App() {
  const inputRef = useRef(null);
  useEffect(() => {
    inputRef.current.focus();
  }, []);
  return (
    <div>
      <input ref={inputRef} />
    </div>
  );
}
```

### 储存渲染周期之间的共享数据

利用useRef获取上一轮的props或状态

```js
const [count,setCount]=useState(1)
const prevCountRef=useRef(0)
const prevCount=prevCountRef.current
useEffect(()=>{
  prevCountRef.current=count
})

return (
  <div className={styles.container}>
    <p>当前值：{count}</p>
    <p>上次值：{prevCount}</p>
    <button onClick={()=>setCount(count+1)}>+</button>
  </div>
)
```

### 利用useRef获取不变定时器对象

```js
const timerRef=useRef()
useEffect(()=>{
  timerRef.current=setInterval(()=>{
    console.log('hello')
  },1000)
  return ()=>{
    clearInterval(timerRef.current)
  }
},[])
```

## 使用useRef时的注意事项

### 1.组件的每次渲染，useRef的返回值都不变

### 2.ref.current发生变化并不会造成组件的重新渲染

### 3.不可以在render函数中更新ref.current的值

### 4.如果给一个组件设定了ref属性，但值却不是useRef创建的，会报错

### 5.ref.current不可作为其他Hooks的依赖，因为ref是可变的，不会使界面再次渲染

参考资料：

- 2023-09-05 React Hooks开发与实战，3.2
