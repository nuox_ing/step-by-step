# useReducer

useReducer能够读取相关状态同时更新多个状态，组件负责发出行为，useReducer负责更新状态的解耦模式，会使代码逻辑更加清晰，代码行为更易预测，代码性能更高

## question

- forwardRef是用来解决什么问题的
- forwardRef的使用场景

## 语法

```tsx
const [state,dispatch]=useReducer(reducer,initState,initAction)
```

reducer是一个函数类似(state,action)=>newState，它接受传入上一个状态和本次的行为(action)，根据行为状态处理并更新状态
initState是初始化的状态，也就是默认值
initAction是useReducer初次执行时被处理的行为这其实是一种惰性初始化，这么做可以用于计算状态的逻辑提取到reducer外部，为将来重置状态行为提供便利
state是状态值
dispatch是更新state的方法，它接受action作为参数。useReducer只需要调用dispatch(action)方法传入的action即可更新state

## 注意事项

- 对于传入的对象类型，react只会判断引用是否改变
- useEffect的清除函数在每次重新渲染时都会执行，而不是在组件卸载的时候执行(只要effect函数执行，清除函数也执行)

参考资料：

- 2023-09-05 React Hooks开发与实战，3.5
- react官网 [https://react.dev/reference/react/useEffect](https://react.dev/reference/react/useEffect)
