# 事件处理

## question

- 什么是JSX

## 合成事件

1. 进行浏览器兼容，方便跨平台
2. 避免垃圾回收 事件池
3. 方便统一管理

### 与原生事件区别

1. 在JSX元素上添加事件,通过on*EventType这种内联方式添加,命名采用小驼峰式(camelCase)的形式,而不是纯小写(原生HTML中对DOM元素绑定事件,事件类型是小写的)；
2. 无需调用addEventListener进行事件监听，也无需考虑兼容性，React已经封装好了一些的事件类型属性；
3. 使用 JSX 语法时你需要传入一个函数作为事件处理函数，而不是一个字符串；
4. 不能通过返回 false 的方式阻止默认行为。你必须显式的使用 preventDefault；

```js
// DOM
<button onclick="activateLasers()">
  Activate Lasers
</button>

// React
<button onClick={activateLasers}>
  Activate Lasers
</button>

// JS
<form onsubmit="console.log('You clicked submit.'); return false">
  <button type="submit">Submit</button>
</form>
 
//获取真实dom事件
const handleClick = (e) => {
  console.log(e.nativeEvent);//获取真实dom事件
}
```

```js
class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // 为了在回调中使用 this，这个绑定是必不可少的
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  }

  render() {
    return (
      // class 的方法默认不会绑定 this。如果没有绑定 this.handleClick 并把它传入了 onClick，
      // this 的值为 undefined。
      <button onClick={this.handleClick}>
        {this.state.isToggleOn ? 'ON' : 'OFF'}
      </button>
    );
  }
}

ReactDOM.render(
  <Toggle />,
  document.getElementById('root')
);

// 为什么要绑定this
function createElement(dom, params) {
  var domObj = document.createElement(dom);
  domObj.onclick = params.onclick;
  domObj.innerHTML = params.conent;
  return domObj
}
```
