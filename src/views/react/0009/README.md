# useState

## question

- setState是同步还是异步
- setState过之后怎么直接获取值

## 什么是useState

useState是改变状态的开关，能够完成对状态的初始化、读取和更新

```js
//数组第一项是状态值（支持函数，并支持多种类型），第二项是更新状态的函数
const [state,setState]=useState()//如果不设置初始值，默认为undefined

const [name,setName]=useState('张三')

const [age,setAge]=useState(()=>{//以函数形式赋值时，引擎会解析初始值，写成箭头函数，不会立即执行，只在第一次初始化的时候解析函数的返回值，减少计算
    return 18
})
```

## useState异步

修改状态是通过useState返回的修改函数实现的，类似与Class组件中的this.setState(),但是this.setState有回调函数，如果想获取更新后的最新值，可以在回调函数中获取。
但是useState没有返回值，只能使用useEffect

```js
import {useState,useEffect} from 'react'
function App(){
    const [name,setName]=useState('张三')
    const handalClick=(){
        setName('李四')
        console.log(name)//张三
    }
    useEffect(()=>{
        console.log(name)//李四
    },[name])
    return <div>
        <div>{name}</div>
        <button onClick={handalClick}>点击</button>
    </div>
}

```

## useState注意事项

### useState不会自动合并对象，需要使用展开运算符，或者Object.assign()来达成合并更新对象的效果（与Class组件中的setSate不同）

```js
function App(){
    const [info,setInfo]=useState({name:'张三'})
    const handalClick=(){
        //info.age=18 错误写法age赋值不上去，因为react数据是immutable
        //setInfo(info)
        setInfo({...info,age:18})
        setInfo(Object.assgin({age:18},info))
    }
    return <div>
        <div>{info.name}</div>
        <button onClick={handalClick}>点击</button>
    </div>
}
```

### useState会出现更新不及时

```js
export default function Home() {
  const [num,setNum]=useState(1)
  const add=()=>{
    setNum(num+1)
    setNum(num+2)
  }
  return (
    <div className={styles.container}>
      <p>{num}</p>
      <button onClick={add}>click</button>
    </div>
  )
}
//点击一次输出3，（因为set函数遇见相同的set只会执行最后一个，猜测）
//解决这个问题（把第二个参数换成函数）
export default function Home() {
  const [num,setNum]=useState(1)
  const add=()=>{
    setNum((num)=>num+1)
    setNum((num)=>num+2)
  }
  return (
    <div className={styles.container}>
      <p>{num}</p>
      <button onClick={add}>click</button>
    </div>
  )
}

```

### 其他注意事项

1. 最好将useState写在函数的起始位置，方便阅读
2. 禁止让useState出现在代码块中（循环和判断中）
3. useState返回函数（数组第二项），其引用是不会变化的（优化性能）
4. 使用函数改变数据时，若数据和之前数据完全相等（Object.is），则不会重新渲染，（由于Object.is是浅比较，所以当状态是一个对象的时候要小心操作）
5. 使用函数改变数据时，传入的值不会和原来的数据合并而是直接替换（与setState完全不一样）所以要修改对象的时候把之前的值保存下来
6. 要实现强制刷新组件，如果时Class组件（使用forceUpdate）强制刷新，如果是函数组件，使用useState的第二个参数（函数）来实现强制刷新
7. 状态是异步的（多个状态的更改会合并），因此不能信任之前的状态，要使用函数的形式来更新状态
8. 如果状态之间没有必然的联系，应该拆分成多个状态，而不是合并成一个状态

## useState的实现原理

```js
  let lastStates=[]
  let index=0
  function myUseState(initialState){
    lastStates[index]=lastStates[index]===undefined?initialState:lastStates[index]
    const currentIndex=index
    function setState(newState){
      lastStates[currentIndex]=newState
      render()
    }
    return [lastStates[index++],setState]
  }
  function render(){
    index=0
    ReactDOM.render(<App/>,document.getElementById('root'))
  }
  //状态和index是强制关联的，因此不能在if、while、for等代码块中使用useState，否侧会错乱
```

参考资料：

- 2023-09-01 React Hooks开发与实战，第三章第一节useState
