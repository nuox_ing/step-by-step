# JSX

## question

- 什么是JSX
- JSX 与 HTML 有何不同

## 什么是JSX

JSX是 JavaScript 的语法扩展，可让您在 JavaScript 文件内编写类似 HTML 的标记

## JSX 规则

1. 返回单个根元素 要从组件返回多个元素，需用单个父标签包装
2. 标签必须闭合
3. 标签属性名驼峰命名
4. 标签属性必须用引号包裹
5. 标签属性值可以是字符串或者表达式
6. {{并且}}不是特殊的语法：它是一个隐藏在 JSX 大括号内的 JavaScript 对象。

## JSX编译

element=>bable=>React.createElement=>VDOM{} diff VDOM2=>react.render(VDOM,'root')=>生成真实dom
