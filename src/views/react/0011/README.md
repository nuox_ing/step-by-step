# forwardRef

## question

- forwardRef是用来解决什么问题的
- forwardRef的使用场景

## 语法

ref的作用是获取实例，但是如果目标组件是一个自定义的函数组件（Function Component），那么它是没有实例的，此时用ref去传递会报错，因此它无法获取到实例，React。forwardRef就是用来解决这个问题的

```js
//错误示例 React直接会报错
//Warning: Function components cannot be given refs. Attempts to access this ref will fail. Did you mean to use React.forwardRef()?
const Input=(props:any)=>{
  const {ref} =props
  return <input ref={ref}  />
}
export default function Home() {
  const ref:any=useRef(null)
  return (
    <div className={styles.container}>
      <button onClick={()=>{
        ref.current.focus()
      }}>获取焦点</button>
      <Input ref={ref} />
    </div>
  )
}
//正确示例
const Input=forwardRef((props:any,ref:any)=>{//注意子组件的ref不是通过props传递的，而是通过forwardRef的第二个参数传递的
  return <input ref={ref}  />
})
export default function Home() {
  const ref:any=useRef(null)
  return (
    <div className={styles.container}>
      <button onClick={()=>{
        ref.current.focus()
      }}>获取焦点</button>
      <Input ref={ref} />
    </div>
  )
}
```

## 使用forwardRef时的注意事项

### 1.ref必须指向DOM元素

### 2.forwardRef可以应用到高阶组件中

```js
//定义最内部的包装组件
const Content=(props:any)=>{
  return <input value={props.val} ref={props.forwardRef} />
}
//通过memo创建HOC
const Wrapper=forwardRef((props:any,ref)=>{
  //
  const ContentWarp=memo(Content)
  //在Wrapper组件中，将ref通过props向下传递给Content组件
  return <ContentWarp forwardRaef={ref} {...props} />
})
export default function Home() {
  const ref:any=useRef(null)
  return (
    <div className={styles.container}>
      <button onClick={()=>{
        ref.current.focus()
      }}>获取焦点</button>
      <Wrapper ref={ref} val='你好嘛' />
    </div>
  )
}
```

参考资料：

- 2023-09-05 React Hooks开发与实战，3.3
