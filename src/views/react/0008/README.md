# 初识React Hooks

## question

- 什么是React Hooks
- React Hooks有哪些优势
- React Hooks解决了什么问题

## 什么是React Hooks

通俗的讲，Hooks只是一些函数，Hooks可以用于在函数组件中引入状态管理（state）和生命周期方法。如果希望让react函数组件具有类组件的功能，就可以使用Hooks。

## React Hooks有哪些优势

### 1.简洁

### 2.上手简单

React Class难表现为如下几点：

- 生命周期函数难以理解，很难熟练掌握
- 与Redux状态管理相关的概念太多
- 高阶组件（HOC）的使用难度较大

React Hooks：

- MobX取代了Redux
- 基于函数式编程，门槛较低，只需要掌握一些javascript的基础知识
- 与生命周期相关的知识不用学，React Hooks使用全新的理念来管理组件的运作过程
- 与HOC相关的知识不用学，React Hooks能够完美解决HOC的问题，并且更可靠

### 3.代码复用性更好

React组件亢长且难以复用，它们本身包含状态

### 4.与TypeScript结合更简单

### 5.便于重构
