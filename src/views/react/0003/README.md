# 生明周期

## question

- 生明周期

## componentDidMount()

componentDidMount()会在组件挂在（插入DOM树中）立即调用。依赖DOM节点的初始化应该放在这里。如需通过网络请求获取数据，此处实例化请求的好地方。

## componentDidUpdate()

componentDidUpdate(prevProps,prevState,snapshot)//prevProps表示之前的prop
componentDidUpdate()会在更新后立即被调用，首次渲染不会执行此方法
当组件更新后，可以在此处对DOM进行操作。如果你对更新前后的prop进行了比较，也可以渲染在此处进行网络请求

### componentWillUnmount()

componentWillUnmount() 会在组件卸载及销毁之前直接调用。在此方法中执行必要的清理操作，例如，清除 timer，取消网络请求或清除在 componentDidMount() 中创建的订阅等。
componentWillUnmount() 中不应调用 setState()，因为该组件将永远不会重新渲染。组件实例卸载后，将永远不会再挂载它

### shouldComponentUpdate()

shouldComponentUpdate(nextProps，nextState)
根据shouldComponentUpdate()的返回值，判断React组件的输出时否手当前state或props更改的影响，默认行为时state每次发生改变组件都会渲染，大部分情况下，你应该遵循默认行为。
当props或者state发生改变时，shouldComponentUpdate()会在渲染执行之前被调用。默认返回值未true，首次渲染或使用forceUpdate()时不会调用该方法。
此方法仅作为性能优化方式而存在，不要企图依靠此方法来阻止渲染，因为这可能会尝试bug。你应该考虑使用内置PrueComponent组件，而不是手动编写shouldComponentUpdate()，PureComponent会对props和state进行浅层比较，并减少跳过必要更新的可能性。 
