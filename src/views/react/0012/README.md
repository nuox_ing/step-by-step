# useImperativeHandle

## question

- forwardRef是用来解决什么问题的
- forwardRef的使用场景

## 语法

useImperrativeHandle(ref,createHandle,[deps]) 返回undefind

ref:定义current对象的ref属性
createHandle:一个函数，返回一个要公开方法的对象，这个对象会作为ref.current的值
deps:可选参数，如果传递了这个参数，那么只有当这个参数发生变化时，才会重新计算createHandle的值（Object.is浅比较）

## 示例1

通过使用useImperativeHandle，可以让父组件调用子组件的方法,可以控制暴露的颗粒度

```tsx
// Home.tsx
import Child from './Child'
export default function Home() {
  const ref:any=useRef(null)
  return (
    <div className={styles.container}>
      <button onClick={()=>{
        ref.current.focus()
      }}>获取焦点</button>
      <Child ref={ref} cName='张三'/>
    </div>
  )
}
// Child.tsx
import React,{ useRef, useImperativeHandle } from 'react'
const Child=(props:any,ref:any)=> {
  const inputRef=useRef<any>(null)
  useImperativeHandle(ref,()=>{
    return{
      focus:()=>{
        inputRef.current.focus()
      }
    }
  })
  return <input ref={inputRef} type="text" defaultValue={props.cName}/>
}
export default React.forwardRef(Child)
```

## 示例2

父组件调用子组件的方法

```tsx
// Home.tsx
import Child from './Child'
export default function Home() {
  const ref:any=useRef(null)
  return (
    <div className={styles.container}>
      <button onClick={()=>{
        ref.current.say()
      }}>调用say</button>
      <button onClick={()=>{
        ref.current.sleep()
      }}>调用sleep</button>
      <Child ref={ref} cName='张三'/>
    </div>
  )
}
// Child.tsx
import React,{ Ref, useImperativeHandle } from 'react'
const Child=(props:any,ref: Ref<unknown> | undefined)=> {
  useImperativeHandle(ref,()=>{
    return{
      say:()=>console.log('say'),
      sleep:()=>console.log('sleep')
    }
  })
  return <>123</>
}
export default React.forwardRef(Child)
```

## 注意事项

- useImperativeHandle应该与forwardRef一起使用
- 使用useImperativeHandle后可以让父子组件分别有自己的ref，通过React.forwardRef将父组件的ref传递过来，通过useImperative方法来自定义开放给父组件current
- [deps] 钩子函数的第三个参数，如果里面涉及某个变化的值，只有当第三个参数发生改变时，父组件接收到的值才会发生变化

## useImperativeHandle与forwardRef的区别

- forwardRef是用来解决父组件获取子组件的ref的问题(ref转发对外暴露DOM元素的实例)
- useImperativeHandle是用来解决子组件向父组件暴露方法的问题

参考资料：

- 2023-09-05 React Hooks开发与实战，3.4
- react官网 [https://react.dev/reference/react/useImperativeHandle](https://react.dev/reference/react/useImperativeHandle)
