# React组件

## question

- 组件名为什么要大写
- 导入导出一个组件

## 组件为啥要大写，不大写会怎么样

因为React根据首字母是否大写来区分是react组件还是dom元素。React中使用JSX语法，但浏览器无法识别JSX语法，需通过babel对JSX语法进行转义；而如果组件的首字母为小写时，其会被认定为原生DOM标签，创建一个不存在的标签是会报错的。

### 原理

我们在 React 中都是写的 JSX语法，从 JSX语法 到页面上的 真实DOM大概需要经历以下几个阶段：JSX语法 —> 虚拟DOM（JS对象） —> 真实DOM。
因为浏览器是无法识别JSX语法的，因此我们需要通过 babel 对JSX语法进行转义，然后才能生成虚拟DOM对象
babel在进行转义JSX语法时，是调用了 React.createElement() 这个方法，这个方法需要接收三个参数：type, config, children。第一个参数声明了这个元素的类型。
我在创建自定义组件时没有首字母大写。 而 babel 在转义时把它当成了一个字符串 传递进去了；我把首字母大写了，babel 在转义时传递了一个变量进去。
问题就在这里，如果传递的是一个字符串，那么在创建虚拟DOM对象时，React会认为这是一个简单的HTML标签，但是这显然不是一个简单的HTML标签，因此去创建一个不存在的标签肯定是会报错的。

参考链接：[https://www.qycn.com/xzx/article/12297.html](https://www.qycn.com/xzx/article/12297.html)

## 导入导出一个组件

```jsx
//导入 
import Child from './Child'
//导出
export default function Child(){
    return <h1>你好啊</h1>
}
```

## 默认导出 vs 具名导出

```jsx
//默认导入导出
//导入 
import Child from './Child'
//导出
export default function Child(){
    return <h1>你好啊</h1>
}
```

```jsx
//具名导入导出
import {Child} from './Child'
//导出
export function Child(){
    return <h1>你好啊</h1>
}
```

一个jsx文件中只能包含一个默认导出
为了减少在默认导出和具名导出之间的混淆，一些团队会选择只使用一种风格（默认或者具名），或者禁止在单个文件内混合使用。这因人而异，选择最适合你的即可！
