# useLayoutEffect

useEffect 就是我们常说的React中的副作用。执行setState会产生新的更新，而每次更新都会触发useEffect的依赖监听。

## question

- forwardRef是用来解决什么问题的
- forwardRef的使用场景

## 语法

```tsx
function useEffect(
  effect:EffectCallback,
  deps?.:DependencyList
)
```

默认情况下在第一次和每次状态更新之后都会执行useEffect，也就是说，useEffect只在某些值发生改变后执行。每轮渲染结束后会延迟调用useEffect（异步执行，不会阻碍DOM渲染），这保证了执行useEffect的时候DOM都已经更新完毕

effect:

1. 页面渲染后执行，因此可以把ajax请求等放在里面执行
2. 可以返回一个函数，类似于Vue中的destroyed钩子，实际场景：清除定时器或DOM元素的监听

deps:

1. 如果不传每次渲染都执行effect函数或者清理
2. 不传，仅在组件挂载和卸载的时候执行，因为不依赖于props或state的任何值，所以useEffect永远都不会重复执行

## 注意事项

- 对于传入的对象类型，react只会判断引用是否改变
- useEffect的清除函数在每次重新渲染时都会执行，而不是在组件卸载的时候执行(只要effect函数执行，清除函数也执行)

参考资料：

- 2023-09-05 React Hooks开发与实战，3.5
- react官网 [https://react.dev/reference/react/useEffect](https://react.dev/reference/react/useEffect)
