# immer

## question

- immer使用场景

## 对比immutable.js

ImmutableJS，非常棒的一个不可变数据结构的库，可以解决上面的问题，但跟 Immer 比起来，ImmutableJS 有两个较大的不足：

1. 需要使用者学习它的数据结构操作方式，没有 Immer 提供的使用原生对象的操作方式简单、易用；
2. 它的操作结果需要通过toJS方法才能得到原生对象，这使得在操作一个对象的时候，时刻要主要操作的是原生对象还是 ImmutableJS 的返回结果，稍不注意，就会产生问题；

## 使用immer优化react

```js
// 定义state
state = {
  members: [
    {
      name: 'ronffy',
      age: 30
    }
  ]
}

// 如何给member中第一个元素的age+1

// error
this.state.members[0].age++;

// setState
const { members } = this.state;
this.setState({
  members: [
    {
      ...members[0],
      age: members[0].age + 1,
    },
    ...members.slice(1),
  ]
})

// 使用reducer
const reducer = (state, action) => {
  switch (action.type) {
    case 'ADD_AGE':
      const { members } = state;
      return {
        ...state,
        members: [
          {
            ...members[0],
            age: members[0].age + 1,
          },
          ...members.slice(1),
        ]
      }
    default:
      return state
  }
}


// 使用immer
this.setState(produce(draft => {
  draft.members[0].age++;
}))

// 使用immer结合reduce
// 注意： produce 内的 recipe 回调函数的第2个参数与obj对象是指向同一块内存
let obj = {};

let producer = produce((draft, arg) => {
  obj === arg; // true
});
let nextState = producer(currentState, obj);

const reducer = (state, action) => produce(state, draft => {
  switch (action.type) {
    case 'ADD_AGE':
      draft.members[0].age++;
  }
})
```

## 网址

[https://immerjs.github.io/immer/zh-CN/](https://immerjs.github.io/immer/zh-CN/)
