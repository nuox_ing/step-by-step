# 单数代词及a/an的用法

## question

- a/an的用法
- girl boy teacher student doctor book dog chair car apple airplane

## I am a girl

英文句子里，首字母一定要大写，句尾和中文一样都要加上句点.
中文和英文最大的区别在于中文可以省略“一个”，如，“我是女孩”，英文一定要说我是一个女孩

## 1英文句子里必须包含几个重要的元素

1. 开头第一个词的首字母一定要大写。
2. 句尾要加上句点（.）。
3. 句中一定要有动词，如“我是”的“是”在英文里算是动词的一种。
4. 英文必须要有“一个=a”的概念，不能像中文一样省略。

## 2

I （代词）=“我”，代词的功能主要用来代替名词
girl(名词)=“女孩”的意思，名词大多用来代表人或东西的名词。例如，男孩、书本、铅笔都算名词。

## 3

am（动词）=“是”的意思，am在英文里算是动词的一种，属于be动词，因此“我是”=I am
be动词有三种，分别是am、is、are

## 4代词

我是 = I am
你是 = You are
他是 = He is
她是 = She is
它是 = It is
这个是 = This is
那个是 = That is
例： You are a boy.

## 练习1

1. 我是一个男孩
I am a boy.
2. 你是一位老师
You are a teacher.
3. 他是一个学生
He is a student.
4. 她是一位医生
She is a doctor.
5. 它是一本书
It is a book.
6. 它是一只狗
It is a dog.
7. 这是一把椅子
This is a chair.
8. 那是一辆车
That is a car.

## 5

英文单词若开头字母是元音字母（a、e、i、o、u）时，要在前面加上“an”，否则要加上“a”

## 练习2

1. 它是一个苹果
It is an apple.
2. 这是一个苹果
This is an apple.
3. 那是一个苹果
That is an apple.
4. 它是一架飞机
It is an airplane.
5. 这是一架飞机
This is an airplane.
6. 那是一架飞机
That is an airplane.