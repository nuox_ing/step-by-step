# 形容词的用法

## question

- girl boy teacher student doctor book dog chair car apple airplane

## 形容词

主要是用来形容人或事物的外观或状态，让人更清楚句子要表达的意思。
形容词通常放在名词的前面，用来描述名词的外观或状态

## 练习1

1. 你是一个矮的男孩
You are a short boy.
2. 我是一位高的老师
I am a tall teacher.
3. 它是一个胖的学生
He is a fat student
4. 她是一位瘦的医生
She is a thin docter
5. 它是一本好书
It is a good book.
6. 它是一只聪明的狗
It is a smart dog.
7. 这个是一把新的椅子
This is a new chair.
8. 那是一辆大的车
That is a big car.
9. 它是一个苹果
It is an apple
10. 这个是一个小的苹果
This is a small apple //an apple 中间放了开头不是元音的small,an 就必须改成a
11. 那是一个大的苹果
That is a big apple
12. 它是一架飞机
It is an airplane.
13. 这个是一架蓝色的飞机
This is a blue airplane
14. 那个是一架漂亮的飞机
That is a beautiful airplane
15. 我们是快乐的女孩
We are happy girls
16. 我们是忙碌的医生
We are busy doctors.
17. 你们是强壮的男孩
You are strong boys.
18. 你们是懒惰的农夫
You are lazy farmers
19. 他们是愚笨的学生
They are stupid students.
20. 这些是肮脏的狗
These are dirty dogs
21. 这些是干净的猫
These are clean cats
22. 那些是白色的公交车
Those are white busse
23. 那些是旧的小刀
Those are old knives

## this、that、these、those除了本身作为代词，也可做形容词

这个男孩=this boy
那个男孩=that boy
这些男孩=these boy
那些男孩=those boy

## 练习2

1. 这个女孩是快乐的
This girl is happly.
2. 这个医生是忙碌的
This doctor is busy.
3. 那只狗是干净的
That dog is clean.
4. 那只猫是脏的
That cat is dirty.
5. 这本书是好的
This book is good.
6. 这些椅子是坏的
These chairs are bad.
7. 那些公交车是白色的
Those buses are white.
8. 那些书是绿色的
Those books are green.
