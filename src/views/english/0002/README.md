# 复数代词及复数名词的用法

## question

- girl boy teacher student doctor book dog chair car apple airplane

## I am a girl

英文句子里，首字母一定要大写，句尾和中文一样都要加上句点.
中文和英文最大的区别在于中文可以省略“一个”，如，“我是女孩”，英文一定要说我是一个女孩

## 1

两个以上要从is变成are

## 2

我们是 = we are
你们是 = you are
他们是 = they are
这些是 = these are
那些是 = those are

## 3

1. 复数名词后面通常要加s，如：books、dogs、chairs、cars、apples、airplanes
2. 复数名词后面如果是以s结尾的，要加es，如：buses
3. 词尾是y的，要变成ies，如：lady变成ladies、bady变成babies
4. 词尾是f或fe的，要变成ves，如：knife变成knives、leaf变成leaves
5. 特殊不规则名词，如 you 单数复数都是you、child变成children

## 练习

1. 我们是女孩
We are girls.
2. 我们是医生
We are doctors
3. 你们是男孩
You are boys
4. 你们是农夫
They are farmers
5. 他们是学生
They are students
6. 他们是小偷
They are thieves
7. 这些是狗
These are dogs
8. 这些是猫
These are cats
9. 那些是公交车
Those are busse
10. 那些是小刀
Those are knives
